<?php

/**
 * @file
 * Check user accounts expiration by cron.
 */

/**
 * How often users will be pushed to update. Example 1*24*60*60 - every day.
 */
define('USER_EXPIRES_CHECK_INTERVAL', 1*24*60*60);
/**
 * How many users will be processed per cron run
 */
define('ANNUAL_UPDATE_USERS_PROCESSED_PER_RUN', 10);
/**
 * How often users should update account.
 */
define('ANNUAL_UPDATE_EXPIRY_INTERVAL', 365*24*60*60);
/**
 * Starting users notification, that they should update account.
 */
define('ANNUAL_UPDATE_START_NOTIFICATION', 14*24*60*60);

/**
 * Debug mode flag. For manual sript runnig. Will display drupal messages if TRUE.
 */
define('IS_DEBUG_MODE', FALSE);

//init
//change dir to drupal root
chdir('../');
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/cron/cron_user_functions.inc';
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$last_users_push_to_update_time = variable_get('last_users_push_to_expires_check', 0);

if((time() - $last_users_push_to_update_time > USER_EXPIRES_CHECK_INTERVAL) ||  IS_DEBUG_MODE){
  $user_list = cron_get_expired_user_list(array('Trainee Member', 'Faculty Member'), ANNUAL_UPDATE_EXPIRY_INTERVAL - ANNUAL_UPDATE_START_NOTIFICATION);

  cron_push_user_expired_list_to_queue($user_list);
  variable_set('last_users_push_to_expires_check', time());
}
  
cron_notificate_expired_users_from_queue(ANNUAL_UPDATE_USERS_PROCESSED_PER_RUN);