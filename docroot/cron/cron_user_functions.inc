<?php
/**
 * @file
 * Provide functionality for user update, and checking user info expiration
 */

/**
 * Get user list for adding to the update queue
 * 
 * @param array $name
 *   The names of user roles
 * 
 * @return array
 *   Return user info
 */
function cron_get_user_list_for_update($roles = NULL){
  $query = db_select('users', 'u');
  $query->condition('u.uid', 0, '<>')
        ->fields('u', array('uid', 'name', 'status', 'created', 'access', 'mail'));
  
  $query->leftJoin('users_roles', 'ur', 'ur.uid = u.uid');
  $query->leftJoin('role', 'r', 'r.rid = ur.rid');
  $query->fields('r', array('name'));
  
  //user should be active not blocked
  $query->condition('u.status', 1, '=');

  $query->leftJoin('field_data_field_account_hc_profile', 'fhc', 'u.uid = fhc.entity_id');
  //field 'Use HC Profiles for my contact info' checked
  $query->condition('fhc.field_account_hc_profile_value', 1, '=');
  
  $query->leftJoin('field_data_field_account_huid', 'fhuid', 'u.uid = fhuid.entity_id');
  $query->addField('fhuid', 'field_account_huid_value', 'huid');
  //field 'HUID' should be not empty
  $query->isNotNull('fhuid.field_account_huid_value');
  
  if(!empty($roles) && is_array($roles)){
    $query->condition('r.name', $roles, 'IN');
  }  
  
  $result = $query->execute()->fetchAllAssoc('uid');
  return $result;  
}

/**
 * Push array of users info to the DrupalQueue
 * 
 * @param array $list 
 */
function cron_push_user_list_to_update_queue($list) {
  $queue = DrupalQueue::get('UserUpdateQueue');

  //Push all the items into the queue, one at a time.
  foreach ($list as $item) {
    $queue->createItem($item);
  }
}

/**
 * Return value like variable_get() function.
 * 
 * @param string $name
 *   The name of the variable to return.
 * @param $default
 *   The default value to use if this variable has never been set.
 * 
 * @return
 *   The value of the variable. Unserialization is taken care of as necessary.
 */
function cron_variable_get_custom($name, $default = NULL){
  $result =  array_map('unserialize', db_query('SELECT name, value FROM {variable} WHERE name=:name', array(':name' => $name))->fetchAllKeyed());
  return isset($result[$name]) ? $result[$name] : $default;
}

/**
 * Get users id's from queue and retrive information about them from external api
 * than save new info about these users.
 * 
 * @param int $update_users_per_run 
 *   The number of users that will be updated.
 */
function cron_update_users_from_queue($update_users_per_run) {
  $queue = DrupalQueue::get('UserUpdateQueue');
  //Pull items out
  $count = 0;
  while (($user_item = $queue->claimItem()) && $count < $update_users_per_run) {
    //Try saving the data.
    if(cron_save_remote_user_info($user_item->data)) {
    //Good, we succeeded.  Delete the item as it is no longer needed.
      $queue->deleteItem($user_item);
    }
    else {
    //You might want to log this
    }
    ++$count;
  }
}

/**
 * Get data from remote source and save
 * 
 * @param type $item
 * @return boolean 
 *   Return TRUE if data retrieved and saved sucessfuly
 */
function cron_save_remote_user_info($user_item) {
  $account = user_load($user_item->uid, TRUE);
  rules_invoke_event('harvard_pnl_rules_cron_user_update_started', $account);
  return TRUE;
} 

/**
 * Get user list for adding to the update queue that will expire soon
 * 
 * @param array $name
 *   The names of user roles
 * @param int
 *   Time notification interval before account expires 
 * 
 * @return array
 *   Return user info
 */
function cron_get_expired_user_list($roles , $anual_update_start_interval){
  $query = db_select('users', 'u');
  $query->condition('u.uid', 0, '<>')
        ->fields('u', array('uid', 'name', 'mail'));
  
  $query->leftJoin('field_data_field_account_update_date', 'ud', 'ud.entity_id = u.uid');
  $query->addField('ud', 'field_account_update_date_value', 'update_date');
  $query->where('(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(ud.field_account_update_date_value)) > :interval_time', array(':interval_time' => $anual_update_start_interval));
  
  $query->leftJoin('field_data_field_account_approve_status', 'app', 'u.uid = app.entity_id');
  $query->addField('app', 'field_account_approve_status_value', 'status');
  $query->condition('app.field_account_approve_status_value', APPROVE_STATUS_APPROVED);
  
  if(!empty($roles)){
    $query->leftJoin('users_roles', 'ur', 'ur.uid = u.uid');
    $query->leftJoin('role', 'r', 'r.rid = ur.rid');
    $query->condition('r.name', $roles, 'IN');
  }  
  $result = $query->execute()->fetchAllAssoc('uid');
  
  return $result;  
}

/**
 * Push array of users info to the DrupalQueue
 * 
 * @param array $list 
 */
function cron_push_user_expired_list_to_queue($list) {
  $queue = DrupalQueue::get('UserInfoExpired');

  //Push all the items into the queue, one at a time.
  foreach ($list as $item) {
    $queue->createItem($item);
  }
}


/**
 * Get users id's from queue and retrive and send notification message
 */
function cron_notificate_expired_users_from_queue($items_processed) {
  $queue = DrupalQueue::get('UserInfoExpired');
  //Pull items out
  $count = 0;
  while (($item = $queue->claimItem()) && $count < $items_processed) {
    //Try saving the data.
    if(cron_anual_update_start_event($item->data)) {
    //Good, we succeeded.  Delete the item as it is no longer needed.
      $queue->deleteItem($item);
    }
    else {
    //You might want to log to watchdog 
    }
    ++$count;
  }
}

/**
 * Invoke anual update event
 * @param obj $user_item 
 */
function cron_anual_update_start_event($user_item){

  $account = user_load($user_item->uid, TRUE);
  rules_invoke_event('harvard_pnl_rules_event_cron_user_anual_update_time_coming', $account);
  if(IS_DEBUG_MODE){
    pr(drupal_get_messages());
  }
  return TRUE;
}

/**
 * Print data for testing/debugging only
 * 
 * @param type $var 
 */
function pr($var) {
  print '<pre>';
  print_r($var);
  print '</pre>';
}

/**
 * Get list of user who need co cache 
 * @return Object
 */
function harvard_pnl_get_uncached_huid(){
  $query = db_select('users', 'user');
  $query->join('field_data_field_account_huid', '`huid`', 'user.uid = huid.entity_id');
  $query->join('field_data_field_account_hc_profile', '`hc_profile`', 'user.uid = hc_profile.entity_id');
  $query->join('users_roles', '`roles`', 'user.uid = roles.uid');
  $query->join('cache_harvard_user', '`cache`', "cache.cid LIKE CONCAT('harvard_pnl:huid:', huid.field_account_huid_value) ");
  $query->condition('hc_profile.field_account_hc_profile_value', 1);
  $query->condition('cache.expire', 'UNIX_TIMESTAMP(NOW())' ,'<');
  $query->condition(db_or()->condition('roles.rid', TRAINEE_ROLE_ID)
                           ->condition('roles.rid', FACULTY_ROLE_ID));
  $query->fields('user', array('uid'));
  $query->addField('huid', 'field_account_huid_value', 'huid');
  $result = $query->execute()->fetchAll() ;
  return $result;
}

/**
 * Add to queue user who need to add to cache
 * @param Object $user_list 
 */
function cron_push_user_to_cache_queue($user_list){
  $queue = DrupalQueue::get('UserCache');

  //Push all the items into the queue.
  foreach ($user_list as $user) {
    $queue->createItem($user);
  }
}

/**
 * Load user from HC api and saves it in cache
 * @param int $cache_users_per_run 
 */
function cron_get_user_queue_for_cache($cache_users_per_run){
  $queue = DrupalQueue::get('UserCache');
  $i = 0;
  while (($item = $queue->claimItem()) && $i < $cache_users_per_run) {
    //caching the data.
    if(!empty($item->data) && isset($item->data)){
      if(!empty($item->data->huid)){
        $hcProfile = harvard_pnl_HC_profile_send_request($item->data->huid);
        if(!empty($hcProfile)){
          harvard_pnl_set_hc_profile_cache($hcProfile);
        }
      }
    }
    $i++;
  }
}