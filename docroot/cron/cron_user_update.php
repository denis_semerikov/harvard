<?php
/**
 * @file
 * Update users info from external source by cron
 */

/**
 * How often users will be updated. Example 7*24*60*60 - every week.
 */
define('USER_UPDATE_INTERVAL', 7*24*60*60);

/**
 * How many users will be updated per one cron run.
 */
define('UPDATE_USERS_PER_RUN', 20);

//init

//change dir to drupal root
chdir('../');
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/cron/cron_user_functions.inc';
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$last_users_push_to_update_time = variable_get('last_users_push_to_update', 0);

if(time() - $last_users_push_to_update_time > USER_UPDATE_INTERVAL){
  $user_list = cron_get_user_list_for_update(array('Trainee Member', 'Faculty Member'));
  cron_push_user_list_to_update_queue($user_list);
  variable_set('last_users_push_to_update', time());
}

cron_update_users_from_queue(UPDATE_USERS_PER_RUN);


