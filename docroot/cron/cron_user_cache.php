<?php

/**
 * @file
 * Check user accounts expiration by cron.
 */

define('USER_ADD_TO_CACHE_QUEUE_INTERVAL', 1*24*60*60); //once a day 
define('USER_CACHE_INTERVAL', 1*60*10); //once a 10 min
define('USER_CACHE_PER_INTERVAL', 2); 

//init

//change dir to drupal root
chdir('../');
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/cron/cron_user_functions.inc';
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
// add to queue
$last_users_push_to_update_time = variable_get('last_daily_push_users_to_cache_queue', 0);
if(time() - $last_users_push_to_update_time > USER_ADD_TO_CACHE_QUEUE_INTERVAL){
  //uncached list of user
  $user_list =  harvard_pnl_get_uncached_huid();
  cron_push_user_to_cache_queue($user_list);
  variable_set('last_daily_push_users_to_cache_queue', time());
}

//get from queue and insert to cache
$last_users_cache_update = variable_get('last_10_min_push_users_to_cache', 0);
if(time() - $last_users_cache_update > USER_CACHE_INTERVAL){
  //cache user
  cron_get_user_queue_for_cache(USER_CACHE_PER_INTERVAL);
  variable_set('last_10_min_push_users_to_cache', time());
}
