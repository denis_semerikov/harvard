(function($){
  $(document).ready(function(){
    setInterval(function(){
      viewBlock = $('.pane-views-events-block-2');
      nodeBlock = $('.omega-12-twocol-6-6-bricks .pane-node:first');
      viewBlockPadding = viewBlock.css('padding-bottom').slice(0, -2) - 0;
      if ((viewBlock.height() + viewBlockPadding) < nodeBlock.height()) {
        viewBlock.height(nodeBlock.height());
      }
    }, 200);
  });
})(jQuery);