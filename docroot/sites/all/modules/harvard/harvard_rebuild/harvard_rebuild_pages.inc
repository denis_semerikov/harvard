<?php 

/**
* page callback for admin/config/set-list-counts
* 
*/

function listing_items_count_form() {    
  
  $form['events'] = array(
    '#type' => 'fieldset',   
    '#title' => t('Events listing options'), 
    '#weight' => 5, 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );
  
    $form['events']['harvard_events_count'] = array(
    '#type' => 'textfield', 
    '#title' => t('Events count per page'), 
    '#default_value' => variable_get('harvard_events_count', 10), 
    '#description' => "Please enter events count per page.",  
    '#size' => 20, 
    '#maxlength' => 20,
    '#weight' => 5, 
  );
  
  $form['events']['harvard_hp_events_count'] = array(
    '#type' => 'textfield', 
    '#title' => t('Home page events count'), 
    '#default_value' => variable_get('harvard_hp_events_count', 3), 
    '#description' => "Please enter events count for home page.",  
    '#size' => 20, 
    '#maxlength' => 20,
    '#weight' => 5, 
  );
  
  $form['announcements'] = array(
    '#type' => 'fieldset',   
    '#title' => t('Announcements listing options'), 
    '#weight' => 5, 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );
  
  $form['announcements']['harvard_announcements_count'] = array(
    '#type' => 'textfield', 
    '#title' => t('Announcements count per page'), 
    '#default_value' => variable_get('harvard_announcements_count', 10), 
    '#description' => "Please enter announcements count per page.",  
    '#size' => 20, 
    '#maxlength' => 20, 
    '#weight' => 10,
  );
  
  $form['announcements']['harvard_hp_announcements_count'] = array(
    '#type' => 'textfield', 
    '#title' => t('Home page announements count'), 
    '#default_value' => variable_get('harvard_hp_announcements_count', 3), 
    '#description' => "Please enter announcements count for home page.",  
    '#size' => 20, 
    '#maxlength' => 20, 
    '#weight' => 10,
  );   
  
  return system_settings_form($form);
}

function listing_items_count_form_validate($form, &$form_state) {  
  if (!is_numeric($form_state['values']['harvard_hp_announcements_count'])) {
    form_set_error('harvard_hp_announcements_count', t('Home page announements count must be a number'));
  }
  
  if (!is_numeric($form_state['values']['harvard_hp_events_count'])) {
    form_set_error('harvard_hp_events_count', t('Home page events count must be a number'));
  }
  
  if (!is_numeric($form_state['values']['harvard_announcements_count'])) {
    form_set_error('harvard_announcements_count', t('Announcement count per page must be a number'));
  }
  
  if (!is_numeric($form_state['values']['harvard_events_count'])) {
    form_set_error('harvard_events_count', t('Events count per page must be a number'));
  }
  
  $form_state['values']['harvard_hp_announcements_count'] = (int)$form_state['values']['harvard_hp_announcements_count'];
  $form_state['values']['harvard_hp_events_count'] = (int)$form_state['values']['harvard_hp_events_count'];
  $form_state['values']['harvard_announcements_count'] = (int)$form_state['values']['harvard_announcements_count'];
  $form_state['values']['harvard_events_count'] = (int)$form_state['values']['harvard_events_count'];
  
  if (!is_int($form_state['values']['harvard_hp_announcements_count'])) {
    form_set_error('harvard_hp_announcements_count', t('Home page announements count must be an integer'));
  }
  
  if (!is_int($form_state['values']['harvard_hp_events_count'])) {
    form_set_error('harvard_hp_events_count', t('Home page events count must be an integer'));
  }
  
  if (!is_int($form_state['values']['harvard_announcements_count'])) {
    form_set_error('harvard_announcements_count', t('Announcements count per page must be an integer'));
  }
  
  if (!is_int($form_state['values']['harvard_events_count'])) {
    form_set_error('harvard_events_count', t('Events count per page must be an integer'));
  }
}
