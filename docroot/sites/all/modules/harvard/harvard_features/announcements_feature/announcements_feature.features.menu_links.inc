<?php
/**
 * @file
 * announcements_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function announcements_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-header-menu:announcements
  $menu_links['menu-header-menu:announcements'] = array(
    'menu_name' => 'menu-header-menu',
    'link_path' => 'announcements',
    'router_path' => 'announcements',
    'link_title' => 'Announcements',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Announcements');


  return $menu_links;
}
