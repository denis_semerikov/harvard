<?php

class HarvardUserMigration extends XMLMigration {

  
  const PENDING_STATUS = 'Pending';


  public function __construct() {
    parent::__construct();
    $this->description = t('Migrate Harvard users');

    // The source ID here is the one retrieved from each data item in the XML file, and
    // used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
            array(
              'hid' => array(
                'type' => 'int',
                'unsigned' => TRUE,
                'not null' => TRUE,
                'description' => 'Harvard User ID',
              )
            ),
            MigrateDestinationUser::getKeySchema()
    );

    // Source fields available in the XML file.
    $fields = array(
      'hid' => t('Source id'),
      'first_name' => t('First name'),
      'last_name' => t('Last name'),
      'middle_initial' => t('Middle initial'),
      'title' => t('Title'),
      'institution' => t('Institution'),
      'department' => t('Department'),
      'division' => t('Division'),
      'address' => t('Address'),
      'phone' => t('Phone'),
      'fax' => t('Fax'),
      'email' => t('Email'),
      'display_email' => t('Display email'),
      'role' => t('Role'),
      'faculty_parent_hid' => t("Faculty parent hid"),
      'member_status' => t("Member status (Fellow/Student)"),
    );

    // This can also be an URL instead of a file path.
    $xml_folder = DRUPAL_ROOT . '/' . drupal_get_path('module', 'harvard_pnl_migrate') . '/xml/';
    $items_url = $xml_folder . 'users_test.xml';
    $item_xpath = '/users/user';
    $item_ID_xpath = 'hid';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationUser();

    // Make the mappings
    $this->addFieldMapping('name', 'email')->xpath('email');
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    $this->addFieldMapping('pass', 'email')->xpath('email')->dedupe('users', 'pass');
    
    $this->addFieldMapping('field_account_huid', 'hid')->xpath('hid');
    $this->addFieldMapping('field_account_hcat_internal_id', 'internalid')->xpath('internalid');
    $this->addFieldMapping('field_account_hcat_url', 'hcat_url')->xpath('hcat_url');
    $this->addFieldMapping('field_account_first_name', 'first_name')->xpath('first_name');
    $this->addFieldMapping('field_account_last_name', 'last_name')->xpath('last_name');
    $this->addFieldMapping('field_account_middle_initial', 'middle_initial')->xpath('middle_initial');
    $this->addFieldMapping('field_account_title', 'title')->xpath('title');
    $this->addFieldMapping('field_account_institution', 'institution')->xpath('institution');
    $this->addFieldMapping('field_account_department', 'department')->xpath('department');
    $this->addFieldMapping('field_account_division', 'division')->xpath('division');
    $this->addFieldMapping('field_account_address', 'address')->xpath('address');
    $this->addFieldMapping('field_field_account_phone', 'phone')->xpath('phone');
    $this->addFieldMapping('field_account_fax', 'fax')->xpath('fax');
    $this->addFieldMapping('mail', 'email')->xpath('email')->dedupe('users', 'name');
    $this->addFieldMapping('field_account_display_email', 'display_email')->xpath('display_email');
    
    $this->addFieldMapping('field_account_is_new')->defaultValue(0);
    $this->addFieldMapping('status')->defaultValue(1);
    $this->addFieldMapping('field_account_approve_status')->defaultValue(PENDING_STATUS);
    
    $this->addFieldMapping('field_account_member_status', 'member_status')->xpath('member_status')->callbacks(array($this, 'convertStatus'));
    $this->addFieldMapping('field_account_faculty_member', 'faculty_parent_hid')->xpath('faculty_parent_hid')->callbacks(array($this, 'convertFacultyParent'));
    
    $this->addFieldMapping('path')->issueGroup(t('DNM'));
    $this->addFieldMapping('init', 'email')->xpath('email');
    
    $this->addFieldMapping('roles', 'role')->xpath('role')->callbacks(array($this, 'convertRole'));
    
    $this->addUnmigratedDestinations(array(
      'login', 
      'created', 
      'access', 
      'role_names', 
      'picture',
      'timezone',
      'language',
      'theme',
      'signature', 
      'signature_format', 
      'field_account_research_area', 
      'field_account_date_of_approval', 
      'field_account_hc_profile', 
      'field_account_update_date', 
      'field_account_website_url', 
      'field_account_rejected_notes', 
      'field_account_academic_degree', 
      'field_account_notification_email',
      'field_account_expiry_date'));
    $this->addUnmigratedSources(array('role'));

  }


  /**
   * Conver Role name to Drupal role rid
   * @param type $value
   * @return type 
   */
  protected function convertRole($value){
    return $this->getRoleId($value);
  }

  /**
   * Conver Status to lovercase
   * @param type $value
   * @return type 
   */
  protected function convertStatus($value){
    return strtolower($value);
  }
  


/**
   * Conver Faculty Parent to uid
   * @param type $value
   * @return type 
   */
  protected function convertFacultyParent($value){
    $query = Database::getConnection()->select('field_data_field_account_huid', 'h');
    $query->addField('h', 'entity_id', 'uid');
    $query->condition('h.field_account_huid_value', $value);
    return $query->execute()->fetchColumn(0);
  }
  
  
  public function prepare($user, $row) {
    $roleId = $this->getRoleId($row->role);
    
    if ($roleId == FACULTY_ROLE_ID) {
      $user->field_account_faculty_member = NULL;
      $user->field_account_member_status = NULL;
    }
  }

  /**
   * get Drupal role id (rid) by Role name
   * @param string $roleName
   * @return int 
   */
  private function getRoleId($roleName) {
    switch ($roleName) {
      case 'Faculty':
        return FACULTY_ROLE_ID;
        break;
      case 'Trainee':
        return TRAINEE_ROLE_ID;
        break;
      default:
        return TRAINEE_ROLE_ID;
        break;
    }
  }
}