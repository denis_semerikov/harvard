(function($) {
  Drupal.behaviors.termReferenceSortable = {
    attach: function(context, settings) {
      //init select
      $('#sortable2').parents('.field-suffix').siblings('select').hide();
      $select = $('#sortable2:not(.sortable-processed)', context).parents('.field-suffix').siblings('select');
      
      if($select.hasClass('error')){
        $select.parent().find('#sortable2').addClass('error');
      }

      $select.find('option').each(function(index, value){
        $option = $(value);
        if($option.val() != '_none'){

          if($(value).attr('selected')){
            $('#sortable2:not(.sortable-processed)').append('<li class="ui-state-highlight"  id="' + $option.val() + '">' + $option.html() + '</li>');
          } else {
            $('#sortable1:not(.sortable-processed)').append('<li class="ui-state-default"  id="' + $option.val() + '">' + $option.html() + '</li>');
          }
        }
      });

      $("#sortable1, #sortable2").sortable({
       connectWith: ".connectedSortable"
      }).disableSelection();

      var max_elements = $('.sortable-lists').attr('id');

      $("#sortable2").sortable({
        receive:function(event, ui){
          if(max_elements){
            if($(this).sortable('toArray').length > max_elements){
              $("#sortable1").sortable('cancel');
              var $sortable_list  = $('#sortable2').parents('.sortable-lists');
              if($sortable_list.find('.error-message').length < 1){
                $sortable_list.prepend('<div class="error-message">Maximum limit: ' + max_elements + '</div>');
              }
            }
          }
        }
      });

      $("#sortable2").bind("sortupdate", function(event, ui) {
        sortSelect(this);
      });
      $select.addClass('sortable-processed');
      $('#sortable2').addClass('sortable-processed');
      function sortSelect(element){

        var selected = $(element).sortable('toArray');
        var $select = $('#sortable2').parents('.field-suffix').siblings('select');
        $select.find(':selected').attr("selected", "");
        if($(selected).length > 0){
          $(selected.reverse()).each(function(index, value){
            var $curent_option = $select.find('option:[value="'+ value +'"]');
            var $curent_option_clone = $curent_option.clone().attr("selected", "selected");
            $curent_option.remove();
            $select.prepend($curent_option_clone);

          });
        }
      }

    }
  };

})(jQuery);
