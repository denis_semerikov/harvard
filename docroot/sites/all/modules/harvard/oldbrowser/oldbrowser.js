(function($) {
    Drupal.behaviors.oldBrowserInit = {
        attach: function (context, settings) {
            var content = $(Drupal.settings.oldbrowser);
            content.find('div.oldbrowser-close > a').click(function() {
                content.remove();
                $.cookie('oldbrowser', 'itsOk', { path: '/' });
                return false;
            });
            $('body', context).prepend(content);
        }
    }
})(jQuery);