<?php

/*
 * Получает информацию о браузере пользователя
 */
function _oldbrowser_browserDetect() {
    if(ini_get('browscap')) {
        $browserInfo=get_browser();
    }
    else {
        module_load_include('inc', 'oldbrowser', 'oldbrowser.browscap');
        $cachedir = 'public://browscap';
        file_prepare_directory($cachedir, FILE_CREATE_DIRECTORY);
        $bc = new Browscap(drupal_realpath($cachedir));
        if (!file_exists(drupal_realpath($cachedir). '/browscap.ini')) {
            $bc->localFile = drupal_get_path('module', 'oldbrowser') . '/browscap.ini';
        }
        $browserInfo = $bc->getBrowser();
    }

    return $browserInfo;
}

/*
 * Получает доступные версии браузеров из настроек
 */
function _oldbrowser_getBrowsers($browser = NULL) {
    static $browsers;
    if (!isset($browsers)) {
        $browsers = array(
            'ie' => variable_get('oldbrowser_versions_ie', '8'),
            'firefox' => variable_get('oldbrowser_versions_firefox', '3'),
            'opera' => variable_get('oldbrowser_versions_opera', '10'),
            'safari' => variable_get('oldbrowser_versions_safari', '4'),
            'chrome' => variable_get('oldbrowser_versions_chrome', '5'),
        );
    }
    if ($browser) {
        return isset($browsers[$browser]) ? $browsers[$browser] : FALSE;
    }
    return $browsers;
}

/*
 * Получает ссылки на страницы загрузки браузеров из настроек
 */
function _oldbrowser_getLinks($browser = NULL) {
    static $links;
    if (!isset($links)) {
        $links = array(
            'ie' => variable_get('oldbrowser_settings_ie_link', 'http://www.microsoft.com/rus/windows/internet-explorer/'),
            'firefox' => variable_get('oldbrowser_settings_firefox_link', 'http://www.mozilla-europe.org/ru/firefox/'),
            'opera' => variable_get('oldbrowser_settings_opera_link', 'http://ru.opera.com/'),
            'safari' => variable_get('oldbrowser_settings_safari_link', 'http://www.apple.com/ru/safari/'),
            'chrome' => variable_get('oldbrowser_settings_chrome_link', 'http://www.google.com/chrome?hl=ru'),
        );
    }
    if ($links) {
        return isset($links[$browser]) ? $links[$browser] : FALSE;
    }
    return $links;
}

/*
 * Получает title и ссылку на видео из настроек
 */
function _oldbrowser_getSettings($name = NULL) {
    static $settings;
    if (!isset($settings)) {
        $settings = array(
            'title' => t(variable_get('oldbrowser_settings_title', 'Your browser is deprecated.<br />We recommend you to try a new browser.')),
            'videolink' => variable_get('oldbrowser_settings_videolink', 'http://www.whatbrowser.org/ru/'),
        );
    }
    if ($settings) {
        return isset($settings[$name]) ? $settings[$name] : FALSE;
    }
    return $settings;
}

/*
 * Проверяет, устарел ли браузер пользователя
 */
function _oldbrowser_isOldBrowser($currentVersion, $requiredVersion) {
    $currentVersion = $currentVersion ? explode(".", $currentVersion) : array();
    $requiredVersion = $requiredVersion ? explode(".", $requiredVersion) : array();

    foreach ($requiredVersion as $i => $version) {
        if (!isset($currentVersion[$i])) {
            $currentVersion[$i] = 0;
        }
        if ((int)$currentVersion[$i] < (int)$version) {
            return TRUE;
            break;
        }
    }
    return FALSE;
}