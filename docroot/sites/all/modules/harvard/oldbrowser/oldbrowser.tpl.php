<div id="oldbrowser">
    <div class="oldbrowser-close">
        <a href="#">&nbsp;</a>
    </div>
    <div class="oldbrowser-video">
        <?php print t('What is a browser?'); ?><br />
        <a href="<?php print _oldbrowser_getSettings('videolink') ?>" target="_blank">
            <?php print t('Watch video'); ?>
            <span>&nbsp;</span>
        </a>
    </div>
    <div class="oldbrowser-title">
        <?php print _oldbrowser_getSettings('title') ?>
    </div>
    <table class="oldbrowser-icons-table">
        <tr>
            <td>
                <a href="<?php print _oldbrowser_getLinks('chrome'); ?>" target="_blank">
                    <span class="oldbrowser-icons oldbrowser-icon-chrome"></span>
                    <span class="oldbrowser-icons-text">Google Chrome</span>
                </a>
            </td>
            <td>
                <a href="<?php print _oldbrowser_getLinks('safari'); ?>" target="_blank">
                    <span class="oldbrowser-icons oldbrowser-icon-safari"></span>
                    <span class="oldbrowser-icons-text">Apple Safari</span>
                </a>
            </td>
            <td>
                <a href="<?php print _oldbrowser_getLinks('firefox'); ?>" target="_blank">
                    <span class="oldbrowser-icons oldbrowser-icon-firefox"></span>
                    <span class="oldbrowser-icons-text">Mozilla Firefox</span>
                </a>
            </td>
            <td>
                <a href="<?php print _oldbrowser_getLinks('opera'); ?>" target="_blank">
                    <span class="oldbrowser-icons oldbrowser-icon-opera"></span>
                    <span class="oldbrowser-icons-text">Opera</span>
                </a>
            </td>
            <td>
                <a href="<?php print _oldbrowser_getLinks('ie'); ?>" target="_blank">
                    <span class="oldbrowser-icons oldbrowser-icon-explorer"></span>
                    <span class="oldbrowser-icons-text">Internet Explorer</span>
                </a>
            </td>
        </tr>
    </table>
</div>