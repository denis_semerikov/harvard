<?php
/**
 * @file
 * home_page_carousel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function home_page_carousel_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function home_page_carousel_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function home_page_carousel_node_info() {
  $items = array(
    'slide' => array(
      'name' => t('Slide'),
      'base' => 'node_content',
      'description' => t('Slide for home page carousel'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
