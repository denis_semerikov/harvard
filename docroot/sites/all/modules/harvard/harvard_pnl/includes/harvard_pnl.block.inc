<?php
/**
 * @file
 * harvard_pnl.block.inc
 */

/**
 * Implementation hook_block_info().
 */
function harvard_pnl_block_info() {
  $blocks = array();
  $blocks['hpl_search_people'] = array('info' => t('People & Labs: Search People'));
  return $blocks;
}

/**
 * Implementation of hook_block_view().
 */
function harvard_pnl_block_view($delta) {
  $block = array();
  switch ($delta) {
    case 'hpl_search_people':
      $block = harvard_pnl_search_people();
      break;
  }
  return $block;
}

/**
 * View search people block
 * @return array
 *   block 
 */
function harvard_pnl_search_people() {
  $block = array();
  $args = arg();
  $args[2] = isset($args[2]) ? $args[2] : NULL;
  $args[3] = isset($args[3]) ? $args[3] : NULL;
  $block['subject'] = t('Search People');
  $block['content'] = drupal_render(drupal_get_form('harvard_pnl_search_people_form'));
  $block['content'] .= !empty($args[2]) ? theme('new_search_link', array('url' => PEOPLE_AND_LABS_SEARCH_URL)) : NULL;
  return $block;
}