<?php

/**
 * @file
 * harvard_pnl.api.inc
 */

/**
 * Check to see if a user has been assigned a certain role.
 *
 * @param $role
 *   The name of the role you're trying to find.
 * @param $user
 *   The user object for the user you're checking; defaults to the current user.
 * @return
 *   TRUE if the user object has the role, FALSE if it does not.
 */
function harvard_pnl_user_has_role($role, $user = NULL) {
  if ($user == NULL) {
    global $user;
  }
  return (is_array($user->roles) && array_key_exists($role, $user->roles)) ? TRUE : FALSE;
}

/**
 * Check if checked user ($member) has faculty member and this member is current authorised user ($user)
 * @global stdClass $user
 * @param array $member
 * @param stdClass $user
 * @return
 *   boolean
 */
function is_supervision_faculty_member($member, $user = NULL) {
  if ($user == NULL) {
    global $user;
  }
  return (isset($member->field_account_faculty_member[LANGUAGE_NONE][0]['uid']) && $member->field_account_faculty_member[LANGUAGE_NONE][0]['uid'] == $user->uid) ? TRUE : FALSE;
}

/**
 * get keys for email templates
 * @return array
 */
function harvard_pnl_email_templates_map() {
  $templates = array(
    'approve_to_editor' => t("Email to all editors after new faculty user request"),
    'approve_to_faculty' => t("Email to Faculty after new trainee user request"),
    'trainee_user_not_in_hc_profiles' => t("Email to Admin/Editor: Trainee User doesn't exist in HC profiles"),
    'faculty_user_not_in_hc_profiles' => t("Email to Admin/Editor: Faculty User doesn't exist in HC profiles"),
    'user_has_been_rejected' => t("User has been rejected"),
    'annual_update_email_to_trainee_he_has_been_rejected' => t("Email to trainee about his account expiration"),
    'annual_update_email_to_advisor_trainee_has_been_rejected' => t("Email to trainee advisor about trainee account expiration"),
    'annual_update_send_raminder' => t("Send reminder for updating user account"),
  );
  return $templates;
}

/**
 * Get HC Profile data (temporary array)
 * @param int $huid
 * @return array
 */
function harvard_pnl_get_hc_profile_data($huid) {
  $data = NULL;
  // get from cache
  $data = harvard_pnl_get_hc_profile_cache($huid);
  if (empty($data)) {
    // if cache is empty - get via API
    $data = harvard_pnl_HC_profile_send_request($huid);
  }
  // collect address
  if (!empty($data['address'])) {
    $data['address'] = trim($data['address']);
    $data['address'] .= (!empty($data['address'])) ? "\n": "";
    $data['address'] .= $data['address1'] . "\n" .  $data['address2'] . "\n" . $data['address3'] . "\n" . $data['address4'];
  }
  //@TODO: for tests
  $data = (empty($data)) ? array(
    'firstname' => 'Mark ' . rand(1111111, 9999999),
    'lastname' => 'Zuckerberg ' . rand(1111111, 9999999),
    'jobtitle' => 'Assistant professor ' . rand(1111111, 9999999),
    'institutionname' => 'Institution ' . rand(1111111, 9999999),
    'departmentname' => 'Medicine ' . rand(1111111, 9999999),
    'divisionname' => 'Clinical Research ' . rand(1111111, 9999999),
    'address' => 'Boston ' . rand(1111111, 9999999),
    'telephone' => '234-234-9987 ' . rand(1111111, 9999999),
    'fax' => '234-234-9987 ' . rand(1111111, 9999999),
    'profileurl' => 'http://google.com',
    'internalid' => '0000',
  ) : $data;

  return $data;
}

/**
 * Return dependence HC Profile fields from Drupal profile fields
 * @return array
 */
function harvard_pnl_hc_profile_drupal_map() {
  $data = array(
    'firstname' => 'field_account_first_name',
    'lastname' => 'field_account_last_name',
    'jobtitle' => 'field_account_title',
    'institutionname' => 'field_account_institution',
    'departmentname' => 'field_account_department',
    'divisionname' => 'field_account_division',
    'address' => 'field_account_address',
    'telephone' => 'field_account_phone',
    'fax' => 'field_account_fax',
    'profileurl' => 'field_account_hcat_url',
    'internalid' => 'field_account_hcat_internal_id',
  );
  return $data;
}

/**
 * Get Faculty students
 * @param int $faculty_uid
 * @return array
 */
function harvard_pnl_get_faculty_students($faculty_uid) {
  $query = db_select('users', 'u');

  $query->join('field_data_field_account_faculty_member', 'faculty_member', 'u.uid = faculty_member.entity_id');
  $query->join('field_revision_field_account_approve_status', 'approve_status', 'u.uid = approve_status.entity_id');
  $query->join('field_revision_field_account_last_name', 'last_name', 'u.uid = last_name.entity_id');
  $query->join('field_revision_field_account_first_name', 'first_name', 'u.uid = first_name.entity_id');
  $query->leftJoin('field_revision_field_account_middle_initial', 'middle_initial', 'u.uid = middle_initial.entity_id');

  $query->condition('faculty_member.field_account_faculty_member_uid', $faculty_uid);
  $query->condition('approve_status.field_account_approve_status_value', APPROVE_STATUS_APPROVED);

  $query->fields('u', array('uid'));
  $query->addField('first_name', 'field_account_first_name_value', 'first_name');
  $query->addField('last_name', 'field_account_last_name_value', 'last_name');
  $query->addField('middle_initial', 'field_account_middle_initial_value', 'middle_initial');

  return $query->execute()->fetchAll();
}

/**
 * Required profiles fields
 * @return array
 */
function harvard_pnl_profile_required_fields_map($form_state) {

  $required  = array(
    'field_account_first_name',
    'field_account_last_name',
    'field_account_institution',
    'field_account_department',
    'field_account_address'
  );
  if(harvard_pnl_user_has_role(FACULTY_ROLE_ID, $form_state['user'])){
    $required[] = 'field_account_hcat_url';
  }
  return $required;
}

/**
 * HC Profiles fields
 * @return array
 */
function harvard_pnl_hc_profiles_map() {
  return array(
    'field_account_first_name',
    'field_account_last_name',
    'field_account_title',
    'field_account_institution',
    'field_account_department',
    'field_account_division',
    'field_account_address',
    'field_account_phone',
    'field_account_fax',
    'field_account_hcat_url',
    'field_account_hcat_internal_id',
  );
}

/**
 * Run drupal user auth after HC PIN user Auth
 * @global stdClass $user
 * @param int $huid
 * @param string $email
 * @param int $role
 *   Drupal rid
 * @param string $trainee_status
 */
function harvard_pnl_after_auth_HC($huid, $email, $role = TRAINEE_ROLE_ID, $trainee_status = TRAINEE_STATUS_STUDENT) {
  global $user;
  //if user doesn't exist (HUID) (if doesn't exist in drupal database)
  if (!harvard_pnl_check_if_user_exist($huid)) {
    // Determine the roles of our new user
    $new_user_roles = array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    );

    switch ($role) {
      case TRAINEE_ROLE_ID:
        $new_user_roles[TRAINEE_ROLE_ID] = TRAINEE_ROLE_ID;
        break;
      case FACULTY_ROLE_ID:
        $new_user_roles[FACULTY_ROLE_ID] = FACULTY_ROLE_ID;
        break;
      case UNSELECTED_ROLE_ID:
        $new_user_roles[UNSELECTED_ROLE_ID] = UNSELECTED_ROLE_ID;
        break;
    }

    require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
    // Create a new user
    $new_user = new stdClass();
    $new_user->name = $email;
    $new_user->pass = user_hash_password(trim($email));
    $new_user->mail = $email;
    $new_user->roles = $new_user_roles;
    $new_user->status = 1; // omit this line to block this user at creation
    $new_user->is_new = TRUE; // not necessary because we already omit $new_user->uid
    $new_user->field_account_huid[LANGUAGE_NONE][0]['value'] = $huid;
    $new_user->field_account_member_status[LANGUAGE_NONE][0]['value'] = $trainee_status;
    user_save($new_user);

    // auth user in drupal
    $user = harvard_pnl_auth_user($email);
    rules_invoke_event('harvard_pnl_rules_event_autorized_in_hc', $user);
  }
  else {
    $username = harvard_pnl_get_username_by_huid($huid);
    $user = harvard_pnl_auth_user($username);
    rules_invoke_event('harvard_pnl_rules_user_login_from_hc', $user);
  }
}

/**
 * check if user exist in drupal
 * @param int $huid
 * @return boolean
 */
function harvard_pnl_check_if_user_exist($huid) {
  $query = db_select('field_data_field_account_huid', 'h');
  $query->condition('field_account_huid_value', $huid);
  $query->addField('h', 'field_account_huid_value', 'huid');
  $result = $query->execute()->fetchAll();
  return !empty($result) ? TRUE : FALSE;
}

/**
 * Auth user in drupal by username
 * @global stdClass $user
 * @param string $username
 * @return stdClass
 */
function harvard_pnl_auth_user($username, &$edit = array()) {
  global $user;
  $user = user_load_by_name($username);
  // Log user in.
  $form_state = array();
  user_login_finalize($form_state);
  return $user;
}

/**
 * Get user name by given harvard profile huid
 * @param int $huid
 * @return string
 *   user name
 */
function harvard_pnl_get_username_by_huid($huid) {
  $query = db_select('users', 'u');
  $query->leftJoin('field_data_field_account_huid', 'h', 'h.entity_id = u.uid');
  $query->condition('h.field_account_huid_value', $huid);
  $query->addField('u', 'name', 'name');
  $result = $query->execute()->fetchField();
  return $result;
}

function harvard_pnl_emulate_huid_login_through_api() {
  drupal_add_css('#login-buttons{left: 40px;position: absolute;top: 60px;}', 'inline');
  $output = '<div id="login-buttons" >';
  $form = drupal_get_form('hiud_login_form');
  $output .= drupal_render($form);
  $output .= '</div>';
  return $output;
}

function emulate_root_login_submit($form, $form_state) {
  global $user;
  $user = user_load(1);
  // Log user in.
  $form_state = array();
  user_login_finalize($form_state);
}

function emulate_with_huid_login_submit($form, $form_state) {
  $huid = '111';
  $email = 'template_email' . $huid . EMPTY_EMAIL_TEMPLATE;
  harvard_pnl_after_auth_HC($huid, $email);
}

function emulate_without_huid_login_submit($form, $form_state) {
  $huid = rand(999, 9999);
  $email = 'template_email' . $huid . EMPTY_EMAIL_TEMPLATE;
  harvard_pnl_after_auth_HC($huid, $email, UNSELECTED_ROLE_ID);
}

function hiud_login_form() {
  $form['submit']['as_admin'] = array(
    '#type' => 'submit',
    '#value' => t('Login as admin'),
    '#name' => 'button-0',
    '#submit' => array('emulate_root_login_submit'),
  );
  $form['submit']['with_huid'] = array(
    '#type' => 'submit',
    '#value' => t('Login with HUID'),
    '#name' => 'button-1',
    '#submit' => array('emulate_with_huid_login_submit'),
  );
  $form['submit']['without_huid'] = array(
    '#type' => 'submit',
    '#value' => t('Login without HUID in drupal DB'),
    '#name' => 'button-2',
    '#submit' => array('emulate_without_huid_login_submit'),
  );
  $form['#theme_wrappers'] = array('form');
  return $form;
}

/**
 * Get available replacement tokens for custom email templates.
 * Hardcoded because we can't get rules state in admin settings page!
 * @return string
 */
function harvard_pnl_get_replacement_patterns_for_rules_email() {
  $header = array('Token', 'Label', 'Description');
  $rows = array(
    array('[site:name]', 'Name', 'The name of the site.'),
    array('[site:mail]', 'Email', 'The administrative email address for the site.'),
    array('[site:url]', 'URL', 'The URL of the site\'s front page.'),
    array('[account:mail]', 'Email', 'The email address of the user account.'),
    array('[account:url]', 'URL', 'The URL of the account profile page.'),
    array('[account:edit-url]', 'Edit URL', 'The URL of the account edit page.'),
    array('[account:field_account_first_name]', 'First name', 'First name of the user'),
    array('[account:field_account_last_name]', 'Last name', 'Last name of the user'),
    array('[account:field_account_approve_status]', 'Approve status', 'User status'),
    array('[account:field-account-expiry-date]', 'Expiry date', 'Expiry date for Trainee'),
    array('[account:field-account-rejected-notes]', 'Rejected notes', 'Rejected notes for user'),
  );

  return theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('width' => '100%')
      ));
}