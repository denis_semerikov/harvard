<?php

/**
 * @file
 * harvard_pnl.hc_profile_api.inc
 */

/**
 * Get Harvard Profile data 
 */
function harvard_pnl_HC_profile_send_request($huid, $replyCount = 3) {
  $url = variable_get('harvard_pnl_hc_profile_url', '');
  $req = file_get_contents(drupal_get_path('module', 'harvard_pnl') . "/includes/HC_profile_API/person.xml");
  $req = str_replace('{huid}', $huid, $req);
  $data = array();

  for ($i = 1; $i <= $replyCount; $i++) {
    $result = drupal_http_request($url, array(
      'headers' => array(
        "Content-Type" => "application/xml",
        "Content-Length" => strlen($req)
      ),
      'method' => "POST",
      'data' => $req)
    );

    if ($result->code == 200) {
      $parser = drupal_xml_parser_create($result->data);
      $values = array();
      xml_parse_into_struct($parser, $result->data, $values);
      foreach ($values as $entry) {
        $data[strtolower($entry['tag'])] = $entry['value'];
      }
      // add to cache
      harvard_pnl_set_hc_profile_cache($data);
      xml_parser_free($parser);
      break;
    }
  }
  return !empty($data['personid']) ? $data : FALSE;
}

/**
 * Get drupal ser id by harvard user id
 * @param int $huid 
 */
function harvard_pnl_get_uid_by_huid($huid){
  
  $query = db_select('field_data_field_account_huid', 'field_huid');
  $query->condition('field_account_huid_value', $huid, '=')
        ->fields('field_huid', array('entity_id'));
  $result = $query->execute()->fetchField();
  $uid = $result;
  return $uid;
}

/**
 * Get HC profile from Drupal cache
 * @param int $huid
 * @return array
 */
function harvard_pnl_get_hc_profile_cache($huid) {
  if(!empty($huid) && is_numeric($huid)){
    $cid = 'harvard_pnl:huid:' . $huid;
    $cid_list = array($cid);
    if ($cache = cache_get_multiple($cid_list, 'cache_harvard_user')) {
      if (isset($cache[$cid]->expire) && !empty($cache[$cid]->expire)){
        if (($cache[$cid]->expire > time()) ){
          $hcProfile = $cache[$cid]->data;
          return $hcProfile;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Set Hc Profile to Drupal cache
 * @param array $hcProfile
 * @return boolean 
 */
function harvard_pnl_set_hc_profile_cache($hcProfile) {
  // one day expire date
  $expire = time()+(60*60*24);
  if(!empty($hcProfile['personid'])){
    $huid = $hcProfile['personid'];
    cache_set('harvard_pnl:huid:' . $huid, $hcProfile, 'cache_harvard_user', $expire);
  }
  return;
}

/**
 * Implements hook_user_operations().
 */
function harvard_pnl_user_operations($form = array(), $form_state = array()) {
  $operations = array(
    'set_nit_is_new' => array(
      'label' => t('Set NOT is new user'),
      'callback' => 'harvard_pnl_user_set_not_is_new',
    ),
  );
  return $operations;
}
 
/**
 * update user account as NOT new
 * @param Object $accounts 
 */
function harvard_pnl_user_set_not_is_new($accounts){
  $accounts = user_load_multiple($accounts);
  foreach ($accounts as $account) {
    // Skip blocking user if they are blocked.
    if ($account !== FALSE && $account->status == 1) {
      $edit = array(
        'field_account_is_new' => array(
          LANGUAGE_NONE => array(
            0 => array(
              'value' => 0,
            ),
          ),
        ),
      );
      user_save($account, $edit); 
      watchdog('user', 'update user ' . $account->uid . ' as NOT new');
    }
  }
}

//80113
//76590
//55089
//867