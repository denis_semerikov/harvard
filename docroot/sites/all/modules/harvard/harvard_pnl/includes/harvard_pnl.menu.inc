<?php
/**
 * @file
 * harvard_pnl.menu.inc
 */

/**
 * Implements hook_menu().
 */
function harvard_pnl_menu() {
  $items = array();
  $items['user/get-email'] = array(
    'title' => 'Get user Email as Image',
    'access arguments' => array('access user profiles'),
    'page callback' => 'harvard_pnl_get_user_email_as_image',
    'page arguments' => array(2),
    'type' => MENU_CALLBACK,
    'file path' => drupal_get_path('module', 'harvard_pnl') . '/includes',
    'file' => 'harvard_pnl.email.inc',
  );
  $items['coming-soon'] = array(
    'title' => 'Coming Soon',
    'access arguments' => array('access user profiles'),
    'page callback' => 'theme_coming_soon_page',
    'type' => MENU_NORMAL_ITEM,
  );
  
  $items['admin/config/harvard'] = array(
    'title' => 'Harvard site settings',
    'position' => 'right',
    'weight' => -5,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  
  $items['admin/config/harvard/email-settings'] = array(
    'title' => 'Site email templates',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('harvard_pnl_admin_email_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'harvard_pnl.admin.inc',
  );
  
  $items['admin/config/harvard/hc-profile-settings'] = array(
    'title' => 'HC Profile settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('harvard_pnl_admin_hc_profile_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'harvard_pnl.admin.inc',
  );
  
  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function harvard_pnl_menu_alter(&$items) {
  // add access for cancel Faculty and Trainee accounts
  $items['user/%user/cancel']['access callback'] = 'harvard_pnl_user_cancel_access';
  $items['user/%user/cancel/confirm/%/%']['access callback'] = 'harvard_pnl_user_cancel_access';
  //set custom permissions to the "node" site url against default "access content" permission
  $items['node']['access arguments'] = array('pnl access to full node list on node path');
}