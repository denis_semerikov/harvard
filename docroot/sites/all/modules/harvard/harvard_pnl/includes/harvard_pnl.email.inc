<?php

/**
 * @file
 * harvard_pnl.email.inc
 */

/**
 * Get user email as image
 */
function harvard_pnl_get_user_email_as_image($uid) {
  $user = user_load($uid);
  $font = 4;
  $email = $user->mail;

  // Get size of font characters
  $fontwidth = imagefontwidth($font);
  $fontheight = imagefontheight($font);

  // Define total size of border around the text
  $border_x = $fontwidth * 3;
  $border_y = $fontheight / 2;

  // Determine size of image
  $length = (strlen($email) * $fontwidth) + $border_x;
  $height = $fontheight + $border_y;

  // Create image using GD image library
  $im = @ImageCreate($length, $height)
      or die("Cannot Initialize new GD image stream");

  // Set background colour to yellow
  $background_color = ImageColorAllocate($im, 255, 255, 255);

  // Set text colour to black
  $text_color = ImageColorAllocate($im, 0, 0, 0);

  // Write text onto image
  imagestring($im, $font, ($border_x / 2), ($border_y / 2), $email, $text_color);

  // Generate and output PNG image
  header("Content-type: image/png");
  imagepng($im);

  die();
}