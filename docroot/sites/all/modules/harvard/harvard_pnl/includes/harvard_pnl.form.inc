<?php

/**
 * @file
 * harvard_pnl.form.inc
 */

/**
 * implimentation of hook_form_alter()
 */
function harvard_pnl_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'user_register_form':
      // disable ability select and Faculty and Trainee role for one user
      harvard_pnl_disable_select_faculty_trainee_roles($form);
      break;
    case 'user_profile_form':
      menu_set_active_item(PEOPLE_AND_LABS_URL);
      //display role choose form, hide other forms elements
      harvard_pnl_first_choose_role_form($form, $form_state);

      // Only for Faculty and trainee users
      if (harvard_pnl_user_has_role(TRAINEE_ROLE_ID, $form_state['user']) || harvard_pnl_user_has_role(FACULTY_ROLE_ID, $form_state['user'])) {

        //adding ajax processing to "use HC profile checkbox";
        _harvard_pnl_use_hc_profile_ajax_processing($form, $form_state);
        harvard_pnl_empty_email_for_new_user($form, $form_state);
        // custom validate. Needs for getting profile data from HC profile
        $form['#validate'][] = 'harvard_pnl_user_profile_form_validate';

        // hide contact section
        $form['contact']['#access'] = FALSE;
        $form['field_account_date_of_approval']['#access'] = FALSE;
        $form['field_account_update_date']['#access'] = FALSE;
        // hide is new [x] checkbox
        $form['field_account_is_new']['#access'] = FALSE;
        // hide email sending flag (used in algorithm of annual update)
        $form['field_annual_update_email_flag']['#access'] = FALSE;
        // change field weight
        $form['picture']['#weight'] = 8;

        // hide Drupal Password and username
        $form['account']['name']['#access'] = FALSE;
        $form['account']['pass']['#access'] = FALSE;
        $form['account']['current_pass']['#access'] = FALSE;

        // Tranee profile.
        harvard_pnl_hide_tranee_fields($form, $form_state);
        // Faculty profile
        harvard_pnl_hide_faculty_fields($form, $form_state);

        // HC Profile checkbox ("Use HC Profiles for my contact info ")
        harvard_pnl_HC_Profile_fields($form, $form_state);

        // only for Trainee and Faculty accounts
        if (harvard_pnl_user_has_role(TRAINEE_ROLE_ID) || harvard_pnl_user_has_role(FACULTY_ROLE_ID)) {
          //add form confirmation checkox.
          harvard_pnl_confirmation_to_publish_field($form, $form_state);
        }

        // change access for cancel user account for Trainee and Faculty members
        $form['actions']['cancel']['#access'] = harvard_pnl_user_cancel_access($form_state['user']);
        $form['actions']['cancel']['#value'] = t('Remove user');
      }

      // disable ability select and Faculty and Trainee role for one user
      harvard_pnl_disable_select_faculty_trainee_roles($form);
      // Disable editing own rejecting notes for current user
      _harvard_pnl_hide_rejected_notes_for_current_user($form, $form_state);
      break;
  }
}

/**
 * Adding ajax callback to hc profile checkbox checking, and processing ajax
 *
 * @param type $form
 * @param type $form_state
 */
function _harvard_pnl_use_hc_profile_ajax_processing(&$form, &$form_state) {
  $form['#prefix'] = '<div id="ajax-processed-div">';
  $form['#suffix'] = '</div>';

  $form['field_account_hc_profile'][LANGUAGE_NONE]['#ajax'] = array(
    'callback' => 'harvard_pnl_ajax_user_use_hc_checkbox_callback',
    'wrapper' => 'ajax-processed-div',
    'method' => 'replace',
    'event' => 'use_hc_checked', //custom jQuery triger, provided in edit_profile.js
    'effect' => 'fade',
  );

  if ($form_state['triggering_element']['#name'] == 'field_account_hc_profile[und]') {
    $user_huid = NULL;

    if (!empty($form_state['input']['field_account_huid'][LANGUAGE_NONE][0]['value'])) {
      $user_huid = $form_state['input']['field_account_huid'][LANGUAGE_NONE][0]['value'];
    }
    elseif (!empty($form_state['user']->field_account_huid[LANGUAGE_NONE][0]['value'])) {
      $user_huid = $form_state['user']->field_account_huid[LANGUAGE_NONE][0]['value'];
    }

    if (!empty($user_huid)) {
      $hc_profile_fields = harvard_pnl_hc_profiles_map();
      $hc_profile_data = harvard_pnl_get_hc_profile_data($user_huid);
      $hc_profile_map = harvard_pnl_hc_profile_drupal_map();
      if (!empty($hc_profile_data)) {
        foreach ($hc_profile_data as $key => $value) {
          if (isset($hc_profile_map[$key]) && in_array($hc_profile_map[$key], $hc_profile_fields)) {
            if ($key == 'profileurl') {
              $form[$hc_profile_map[$key]][LANGUAGE_NONE][0]['#default_value']['url'] = $value;
              $form[$hc_profile_map[$key]][LANGUAGE_NONE][0]['#value'] = $form[$hc_profile_map[$key]][LANGUAGE_NONE][0]['#default_value'];
            }
            else {
              $form[$hc_profile_map[$key]][LANGUAGE_NONE][0]['value']['#value'] = $value;
            }

            if (isset($hc_profile_map[$key]) && in_array($hc_profile_map[$key], $hc_profile_fields)) {
              $form_state['input'][$hc_profile_map[$key]][LANGUAGE_NONE][0]['value'] = $value;
            }
            if (isset($hc_profile_map[$key]) && in_array($hc_profile_map[$key], $hc_profile_fields)) {
              $form_state['values'][$hc_profile_map[$key]][LANGUAGE_NONE][0]['value'] = $value;
            }
          }
        }
      }
      else {
        drupal_set_message('Error getting info from Harvard profiles.', 'error');
      }
    }
    else {
      drupal_set_message('Empty HUID field.', 'error');
    }
  }
}

/**
 * Disable editing own rejecting notes for current user
 *
 * @param boolean $form
 * @param type $form_state
 */
function _harvard_pnl_hide_rejected_notes_for_current_user(&$form, $form_state){
  global $user;
  if(!empty($form['field_account_rejected_notes'])) {
    if($user->uid == $form_state['user']->uid){
      $form['field_account_rejected_notes']['#disabled'] = TRUE;
    }
  }
}

/**
 * Display role choose form, hide other forms elements
 */
function harvard_pnl_first_choose_role_form(&$form, &$form_state) {
  if (!empty($form_state['user']->roles[UNSELECTED_ROLE_ID])) {
    $form_childrens = element_children($form);
    $allowed_childrens = array('form_token', 'form_id', 'form_build_id', 'actions');
    foreach ($form_childrens as $child_name) {
      if (!in_array($child_name, $allowed_childrens)) {
        $form[$child_name]['#access'] = FALSE;
      }
    }
    $form['role_select'] = array(
      '#type' => 'select',
      '#title' => t('Select Your role'),
      '#options' => array(
        FACULTY_ROLE_ID => $form['account']['roles']['#options'][FACULTY_ROLE_ID],
        TRAINEE_ROLE_ID => $form['account']['roles']['#options'][TRAINEE_ROLE_ID],
      ),
      '#default_value' => 0,
    );

    $form['#submit'] = array(0 => 'harvard_pnl_first_choose_role_submit');
    $form['#validate'][] = 'harvard_pnl_first_choose_role_validate';
  }
}
/**
 * Validate first role choosing form
 */
function harvard_pnl_first_choose_role_validate($form, &$form_state){
  if($form_state['values']['role_select'] != FACULTY_ROLE_ID && $form_state['values']['role_select'] != TRAINEE_ROLE_ID){
    form_set_error('role_select', t('Invalide role value'));
  }
}

/**
 * Process first role choosing form
 */
function harvard_pnl_first_choose_role_submit($form, &$form_state) {
  global $user;
  $curent_roles = $user->roles;
  $new_role = $form_state['values']['role_select'];

  if(!empty($form_state['user']->roles[UNSELECTED_ROLE_ID])) {
    unset($form_state['user']->roles[UNSELECTED_ROLE_ID]);
    $form_state['user']->roles[$new_role] = $form['account']['roles']['#options'][$new_role];
  }
  if(!empty($curent_roles[UNSELECTED_ROLE_ID])) {
    unset($curent_roles[UNSELECTED_ROLE_ID]);
  }
  $curent_roles[$new_role] = $form['account']['roles']['#options'][$new_role];
  $user_edit = array('roles' => $curent_roles);
  if($form_state['values']['role_select'] == FACULTY_ROLE_ID && isset($form_state['user']->field_account_hc_profile[LANGUAGE_NONE])){
    $user_edit['field_account_hc_profile'][LANGUAGE_NONE][0]['value'] = 1;
  }
  user_save($form_state['user'], $user_edit);
  rules_invoke_event('harvard_pnl_rules_user_selected_role', $user);
}

/**
 * Validate for user edit form (Required fields)
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_user_profile_form_validate($form, &$form_state) {
  $is_hc_profile = (isset($form_state['values']['field_account_hc_profile'][LANGUAGE_NONE][0]['value'])) ? $form_state['values']['field_account_hc_profile'][LANGUAGE_NONE][0]['value'] : NULL;
  // if not selected "Use HC Profiles for my contact info" add validate
  if (!$is_hc_profile) {
    $required_elements = harvard_pnl_profile_required_fields_map($form_state);
    foreach ($required_elements as $element) {
      if ($form_state['values'][$element][LANGUAGE_NONE][0]['value'] == '' && empty($form_state['values'][$element][LANGUAGE_NONE][0]['url'])) {
        form_set_error($element, t('!name field is required.', array('!name' => $form[$element][LANGUAGE_NONE]['#title'])));
      }
    }
  }
  // check roles (Trainee Member and Faculty Member) only one can be selected at once
  if(!empty($form_state['values']['roles_change']) &&
      (in_array(TRAINEE_ROLE_ID, $form_state['values']['roles_change']) && in_array(FACULTY_ROLE_ID, $form_state['values']['roles_change']))){
      form_set_error('roles', t('You can`t select two roles at once (Trainee Member and Faculty Member)'));
  }
  if (!harvard_pnl_user_has_role(TRAINEE_ROLE_ID) && harvard_pnl_user_has_role(TRAINEE_ROLE_ID, $form_state['user'])) {
    $exp_date = (isset($form_state['values']['field_account_expiry_date'][LANGUAGE_NONE][0]['value'])) ? $form_state['values']['field_account_expiry_date'][LANGUAGE_NONE][0]['value'] : NULL;
    if (empty($exp_date)) {
      form_set_error('field_account_expiry_date', t('!name field is required.', array('!name' => $form['field_account_expiry_date'][LANGUAGE_NONE]['#title'])));
    }
  }
}
/**
 * Ajax callback for getting info from HC profiles, after checking Use HC Profiles checkbox
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function harvard_pnl_ajax_user_use_hc_checkbox_callback($form, $form_state) {
  return $form;
}

/**
 * Add rules for profile form if checked HC Profiles checkbox
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_HC_Profile_fields(&$form, $form_state) {
  $hc_profile_fields = harvard_pnl_hc_profiles_map();
  $required_elements = harvard_pnl_profile_required_fields_map($form_state);


  foreach ($hc_profile_fields as $field) {
    if (isset($form[$field])) {
      $form[$field]['#attributes']['class'][] = 'hc-profile-fields';
    }
  }
  //$form["account"]['mail']['#attributes']['class'][] = 'hc-profile-fields';

  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'harvard_pnl') . '/js/edit_profile.js',
    'type' => 'file'
  );

    // add states for required fileds
    foreach ($required_elements as $field) {
      if (isset($form[$field])) {
        $form[$field]['#attributes']['class'][] = 'hc-required';
      }
    }


  // add disable status
  foreach ($hc_profile_fields as $field) {
    if (isset($form[$field])) {
      if (harvard_pnl_user_has_role(FACULTY_ROLE_ID, $form_state['user']) && !harvard_pnl_user_has_role(ADMIN_ROLE_ID)) {
        // disable for faculty
        $form[$field]['#disabled'] = TRUE;
      }
    }
  }


}

/**
 * Hide/disable fields on user-edit form for Trainee members
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_hide_tranee_fields(&$form, $form_state) {
  if (harvard_pnl_user_has_role(TRAINEE_ROLE_ID, $form_state['user'])) {

    //hide tranee form for Faculty member if it was not his trainee
    if (!is_supervision_faculty_member($form_state['user']) && harvard_pnl_user_has_role(FACULTY_ROLE_ID)) {
      $form['#access'] = FALSE;
    }

    // Disable approve field
    if (!user_access('pnl can change Trainee approve status') ||
        (!is_supervision_faculty_member($form_state['user']) && harvard_pnl_user_has_role(FACULTY_ROLE_ID))) {
      $form['field_account_approve_status']['#disabled'] = TRUE;
    }
    // add rules for rejection status
    harvard_pnl_add_rejected_state_rules($form);

    // Remove "None" oprion for approve status
    unset($form['field_account_approve_status'][LANGUAGE_NONE]['#options']['_none']);

    // Disable member status field
    if (!user_access('pnl can change member status for Trainee members') ||
        (!is_supervision_faculty_member($form_state['user']) && harvard_pnl_user_has_role(FACULTY_ROLE_ID))) {
      $form['field_account_member_status']['#disabled'] = TRUE;
    }
    else {
      // Remove "None" oprion for member status
      unset($form['field_account_member_status'][LANGUAGE_NONE]['#options']['_none']);
    }

    // Disable expiry date field and display field value in markup
    if (!user_access('pnl can change expiry date for Trainee members') ||
        (!is_supervision_faculty_member($form_state['user']) && harvard_pnl_user_has_role(FACULTY_ROLE_ID))) {

      if(!empty($form['field_account_expiry_date'][LANGUAGE_NONE][0]['#default_value']['value'])){
        $form['field_account_expiry_date_markup'] = $form['field_account_expiry_date'];
        $date = new DateTime($form['field_account_expiry_date'][LANGUAGE_NONE][0]['#default_value']['value']);
        $date->format('Y-m-d H:i:s');
        $form['field_account_expiry_date_markup'][LANGUAGE_NONE] = array('#markup' => $date->format('m/d/Y'));
        $markup_title = !empty($form['field_account_expiry_date'][LANGUAGE_NONE][0]['#tittle']) ? $form['field_account_expiry_date'][LANGUAGE_NONE][0]['#tittle'] : t('Expiry date') ;
        $form['field_account_expiry_date_markup'][LANGUAGE_NONE]['#prefix'] = '<div class="expire-date-title"><b>' . $markup_title . '</b></div>';
      }

      $form['field_account_expiry_date']['#access'] = FALSE;
    }

    // approve confirm message
    if (is_supervision_faculty_member($form_state['user'])) {
      $confirm_message_title = t("I have reviewed the request and confirm that this trainee is a member of my lab.  I have confirmed the expiry period is reasonable given this individual’s expected tenure in my lab.");
      $form['confirm_message'] = array(
        '#type' => 'checkbox',
        '#title' => $confirm_message_title,
        '#weight' => 30,
        '#states' => array(
          'visible' => array(
            ':input[name="field_account_approve_status[und]"]' => array('value' => APPROVE_STATUS_APPROVED),
          ),
        ),
      );
      $form['#validate'][] = 'harvard_pnl_approve_user_confirm_message_validate';
    }

    //access to changing faculty member
    if (!harvard_pnl_user_has_role(TRAINEE_ROLE_ID)) {
        // Disable member status field
      if (!user_access('pnl can change Faculty member for Trainee members') ||
          (!is_supervision_faculty_member($form_state['user']) && harvard_pnl_user_has_role(FACULTY_ROLE_ID))) {
        $form['field_account_faculty_member']['#disabled'] = TRUE;
      }
    }
  }
  else {
    // hide expiry date for all users except Trainee members
    $form['field_account_expiry_date']['#access'] = FALSE;
  }
}

/**
 * Hide/disable fields on user-edit form for Faculty members
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_hide_faculty_fields(&$form, $form_state) {
  if (harvard_pnl_user_has_role(FACULTY_ROLE_ID, $form_state['user'])) {
    // required  "Use HC Profiles for my contact info " for Faculty

    if (!harvard_pnl_user_has_role(ADMIN_ROLE_ID)) {
      //$form['field_account_hc_profile'][LANGUAGE_NONE]['#required'] = TRUE;
      $form['field_account_hc_profile'][LANGUAGE_NONE]['#disabled'] = TRUE;
    }

    // Disable approve field
    if (!user_access('pnl can change Faculty approve status')) {
      $form['field_account_approve_status']['#disabled'] = TRUE;
    }

    // add rules for rejection status
    harvard_pnl_add_rejected_state_rules($form);

    // Remove "None" oprion for approve status
    unset($form['field_account_approve_status'][LANGUAGE_NONE]['#options']['_none']);

    // approve confirm message
    if (harvard_pnl_user_has_role(EDITOR_ROLE_ID)) {
      $confirm_message_title = t("I have reviewed this request and based on the information contained have confirmed that this faculty member is conducting immunological research.");
      $form['confirm_message'] = array(
        '#type' => 'checkbox',
        '#title' => $confirm_message_title,
        '#weight' => 30,
        '#states' => array(
          'visible' => array(
            ':input[name="field_account_approve_status[und]"]' => array('value' => APPROVE_STATUS_APPROVED),
          ),
        ),
      );
      $form['#validate'][] = 'harvard_pnl_approve_user_confirm_message_validate';
    }
  }
}
/**
 * Empty system generated email value for new user. User should input own value.
 * @param type $form
 * @param type $form_state
 */
function harvard_pnl_empty_email_for_new_user(&$form, $form_state) {
  if(!empty($form['account']['mail']['#default_value']) && strpos($form['account']['mail']['#default_value'], EMPTY_EMAIL_TEMPLATE) !== FALSE){
    $form['account']['mail']['#default_value'] = "";
  }
}

/**
 * Add cechbox user confirm to pulish his info
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_confirmation_to_publish_field(&$form, $form_state) {
  if (isset($form['#user']->field_account_is_new[LANGUAGE_NONE][0]['value']) && $form['#user']->field_account_is_new[LANGUAGE_NONE][0]['value'] == 1) {
    $confirm_to_publish_title = t('I agree to allow my information ');
    $confirm_to_publish_description = t('to be presented to the general public on this website. If I at some point in the future elect to remove myself from university directories, it will be my responsibility to also request to be removed from this directory if appropriate.  If I am a student, I have provided this information willingly and it is not being extracted from any official Harvard records.');
    $form['allow_user'] = array(
      '#type' => 'checkbox',
      '#title' => $confirm_to_publish_title,
      '#description' => $confirm_to_publish_description,
      '#options' => array(
        '1' => t('Yes'),
        '0' => t('No')
      ),
      '#required' => TRUE,
      '#weight' => 30,
    );
  }
}

/**
 * Add rules for rejection status.
 * @param array $form
 */
function harvard_pnl_add_rejected_state_rules(&$form) {
  $form['field_account_rejected_notes']['#states'] = array(
    'visible' => array(
      ':input[name="field_account_approve_status[und]"]' => array('value' => 'rejected'),
    ),
    'required' => array(
      ':input[name="field_account_approve_status[und]"]' => array('value' => 'rejected'),
    ),
  );
}

/**
 * Validate for approve user comfirm checkbox
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_approve_user_confirm_message_validate($form, &$form_state) {
  $approve_status = $form_state['values']['field_account_approve_status'][LANGUAGE_NONE][0]['value'];
  $confirm_message = $form_state['values']['confirm_message'];

  if (empty($confirm_message) && $approve_status == APPROVE_STATUS_APPROVED) {
    form_error($form['confirm_message'], t('The "!name" option is required.', array('!name' => $form['confirm_message']['#title'])));
  }
}

/**
 * Disable ability to select both roles (Faculty, Trainee) for one user.
 * @param array $form
 */
function harvard_pnl_disable_select_faculty_trainee_roles($form) {
  if (isset($form['account']['roles'])) {
    drupal_add_js(array(
      'pnlFacultyRoleId' => FACULTY_ROLE_ID,
      'pnlTraineeRoleId' => TRAINEE_ROLE_ID
        ), 'setting');
    drupal_add_js(base_path() . drupal_get_path('module', 'harvard_pnl') . '/js/unique_role.js');
  }
}

/**
 * Custom submission user, before default
 * @param array $form
 * @param array $form_state
 */
function custom_harvard_pnl_user_submit($form, &$form_state) {
  if ($form_state['values']['field_account_role'] == FACULTY_STATUS) {
    // save as faculty
    $form_state['values']['roles'][FACULTY_ROLE_ID] = TRUE;
    $form_state['values']['roles'][TRAINEE_ROLE_ID] = FALSE;
  }
  else {
    //save as trianee
    $form_state['values']['roles'][TRAINEE_ROLE_ID] = TRUE;
    $form_state['values']['roles'][FACULTY_ROLE_ID] = FALSE;
  }
}

/**
 * Menu access callback; limit access to account cancellation pages.
 *
 * Limit access to users with the 'cancel account' permission or administrative
 * users, and prevent the anonymous user from cancelling the account and
 * add access for cancel Faculty and Trainee accounts
 */
function harvard_pnl_user_cancel_access($account) {
  if (user_access('pnl can cancel member account')) {
    // can cancel Tranee account
    return TRUE;
  }
  return ((($GLOBALS['user']->uid == $account->uid) && user_access('cancel account'))) && $account->uid > 0;
}

/**
 * Browse By Research Area search form
 * @param array $form
 * @param array $form_state
 * @return array
 */
function harvard_pnl_browse_by_research_area_form($form, &$form_state) {
  $voc = taxonomy_vocabulary_load(RESEARCH_AREA_VOC);
  $tree = taxonomy_get_tree($voc->vid);
  $options = array();
  foreach ($tree as $term) {
    $options[$term->tid] = $term->name;
  }

  $args = arg();
  $args[2] = isset($args[2]) ? $args[2] : NULL;

  ctools_add_js('auto-submit');

  $form['people_labs_browse_by_research_area_select'] = array(
    '#type' => 'select',
    '#title' => '',
    '#options' => $options,
    '#default_value' => $args[2],
    '#attributes' => array('class' => array('ctools-auto-submit')),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#name' => 'search_people_browse_by_research_area_button',
    '#attributes' => array('class' => array('ctools-use-ajax', 'ctools-auto-submit-click', 'search-people-btn')),
  );
  return $form;
}

/**
 * Browse By Research Area search form submit
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_browse_by_research_area_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    PEOPLE_AND_LABS_SEARCH_URL . '/all/' . $form_state['values']['people_labs_browse_by_research_area_select'],
  );
}

/**
 * Search people block form
 * @param array $form
 * @param array $form_state
 * @return array
 */
function harvard_pnl_search_people_form($form, &$form_state) {
  $args = arg();
  $args[2] = isset($args[2]) ? $args[2] : NULL;
  $args[3] = isset($args[3]) ? $args[3] : NULL;
  $item = filter_xss($args[3]);
  $item = ($item == 'all') ? '' : $item;

  $form['search_people'] = array(
    '#type' => 'textfield',
    '#name' => 'search_people',
    '#default_value' => $item,
  );
  $form['search_people_button'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#name' => 'search_people_button',
    '#attributes' => array('class' => array('search-people-btn')),
  );
  return $form;
}

/**
 * Search people block form submit
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_search_people_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    PEOPLE_AND_LABS_SEARCH_URL . '/all/all/' . $form_state['values']['search_people'],
  );
}

/**
 * Display people filter form
 * @param array $form
 * @param array $form_state
 * @return array
 */
function harvard_pnl_search_display_form($form, &$form_state) {
  $args = arg();
  $args[2] = isset($args[2]) ? $args[2] : NULL;
  $args[4] = isset($args[4]) ? $args[4] : NULL;
  $args[5] = isset($args[5]) ? $args[5] : NULL;
  $default = 'faculty';

  if ($args[4] == 'all') {
    $default = 'all';
  }
  else if ($args[4] == FACULTY_ROLE_ID) {
    $default = 'faculty';
  }
  elseif ($args[4] == TRAINEE_ROLE_ID) {
    if ($args[5] == TRAINEE_STATUS_FELLOW) {
      $default = 'fellow';
    }
    elseif ($args[5] == TRAINEE_STATUS_STUDENT) {
      $default = 'student';
    }
  }

  ctools_add_js('auto-submit');

  $options = array(
    'all' => t('All'),
    'faculty' => t('Faculty'),
    'fellow' => t('Fellows'),
    'student' => t('Students'),
  );

  $form['search_display'] = array(
    '#type' => 'radios',
    '#name' => 'display_by_role_status',
    '#default_value' => $default,
    '#options' => $options,
    '#attributes' => array('class' => array('ctools-auto-submit')),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#name' => 'search_people_display_button',
    '#attributes' => array('class' => array('ctools-use-ajax', 'ctools-auto-submit-click', 'search-people-btn')),
  );

  return $form;
}

/**
 * Display people filter form submit
 * @param array $form
 * @param array $form_state
 */
function harvard_pnl_search_display_form_submit($form, &$form_state) {
  $value = $form_state['values']['search_display'];

  switch ($value) {
    case 'all':
      $url = harvard_pnl_search_display_build_url('all');
      break;
    case 'faculty':
      $url = harvard_pnl_search_display_build_url(FACULTY_ROLE_ID);
      break;
    case 'student':
    case 'fellow':
      $url = harvard_pnl_search_display_build_url(TRAINEE_ROLE_ID, $value);
      break;
  }

  $form_state['redirect'] = array($url);
}

/**
 * Build url for people search
 * @param array $args
 * @param int $role
 * @param string $status
 * @return string
 */
function harvard_pnl_search_display_build_url($role = NULL, $status = NULL) {
  $args = arg();
  $args[1] = isset($args[1]) ? $args[1] : NULL;

  $url = PEOPLE_AND_LABS_SEARCH_URL . "/" . $args[1];
  $url .= (isset($args[2])) ? "/" . $args[2] : '/all';
  $url .= (isset($args[3])) ? "/" . $args[3] : '/all';
  $url .= (isset($role)) ? "/" . $role : '';
  $url .= (isset($status)) ? "/" . $status : '';
  return $url;
}