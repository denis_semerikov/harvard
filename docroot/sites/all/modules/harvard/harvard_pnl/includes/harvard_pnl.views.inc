<?php

/**
 * @file
 * harvard_pnl.views.inc
 */

/**
 * Implements hook_views_pre_render().
 */
function harvard_pnl_views_pre_render(&$view) {
  switch ($view->name) {
    case "people_and_labs_search":
      $args = arg();
      $view->display_filter = drupal_render(drupal_get_form('harvard_pnl_search_display_form'));

      $view->results_for = '';
      if (isset($args[2]) && $args[2] != 'all') {
        // search by Research Area
        $term = taxonomy_term_load($args[2]);
        $view->results_for = t('for') . " " . $term->name;
        drupal_add_js(base_path() . drupal_get_path('module', 'harvard_pnl') . "/js/jquery.highlight.js");
        drupal_add_js('
          (function ($) {
             $(function () {
               $(".view-content", "#pnl-search-result").highlight("' . $term->name . '");
             });
          })(jQuery);
          ', 'inline');
      }

      break;
  }
}


/**
 * Implements hook_views_query_alter().
 */
function harvard_pnl_views_query_alter(&$view, &$query) {
  switch ($view->name) {
    case "people_and_labs_search":
      // change query for "like" serach in PnL section
      foreach ($query->where as $key => $item) {
        if (isset($item['conditions']) && is_array($item['conditions'])) {
          foreach ($item['conditions'] as $k => $cond) {
            if (isset($cond['field']) && isset($cond['operator']) && $cond['field'] == 'field_data_field_account_last_name.field_account_last_name_value' && $cond['operator'] == "=") {
              $query->where[$key]['conditions'][$k]['operator'] = 'like';
              $query->where[$key]['conditions'][$k]['value'] = '%' . $cond['value'] . '%';
            }
          }
        }
      }
      break;
    case "approve_dashboard":
      // Remove dependence from supervision faculty for ADMIN users
      if (harvard_pnl_user_has_role(ADMIN_ROLE_ID) || harvard_pnl_user_has_role(EDITOR_ROLE_ID)) {
        unset($query->where[0]);
      }
      break;
  }
}

/**
 * Implementation of hook_query_alter().
 */
function harvard_pnl_query_alter(QueryAlterableInterface $query) {
   // altering search people query
  if ($query->hasAllTags('views', 'views_people_and_labs_search')) {
    $arguments = $query->getArguments();
    $conditions = & $query->conditions();
    // check if NOT search by first char
    if (!array_key_exists(':field_data_field_account_last_name_field_account_last_name_value', $arguments) && !array_key_exists(':field_data_field_account_research_area_field_account_research_area_tid', $arguments)) {
      // Reset the condition array.
      $conditions = array('#conjunction' => 'AND');
      //arguments
      $search = _harvard_pnl_get_field_argument($query->alterMetaData['view']->query->where, 'field_data_field_account_last_name.field_account_last_name_value');
      $user_status = _harvard_pnl_get_field_argument($query->alterMetaData['view']->query->where, 'users.status');
      $approve_status = _harvard_pnl_get_field_argument($query->alterMetaData['view']->query->where, 'field_data_field_account_approve_status.field_account_approve_status_value');
      $role_ids = _harvard_pnl_get_field_argument($query->alterMetaData['view']->query->where, 'users_roles.rid');
      $member_status = _harvard_pnl_get_field_argument($query->alterMetaData['view']->query->where, 'field_data_field_account_member_status.field_account_member_status_value = :field_data_field_account_member_status_field_account_member_status_value ');
      // Build new condition
      $condition = db_or()
          ->condition('field_data_field_account_last_name.field_account_last_name_value', $search, 'LIKE')
          ->condition('t.name', $search, 'LIKE');

      // Add the new OR condition to the query
      $query->condition($condition);
      $query->condition('users.status', $user_status, '<>');
      $query->condition('field_data_field_account_approve_status.field_account_approve_status_value', array($approve_status), 'IN');

      if(!empty($role_ids)){
         $query->condition('users_roles.rid', $role_ids);
      }
      if(!empty($member_status)){
         $query->condition('field_data_field_account_member_status.field_account_member_status_value', $member_status);
      }

      $query->addJoin('LEFT', 'field_revision_field_account_research_area', 'ra', 'users.uid = ra.entity_id');
      $query->addJoin('LEFT', 'taxonomy_term_data', 't', 't.tid = ra.field_account_research_area_tid');
      $query->distinct();
    }
  }
}

/**
 * Reqursive field_argument values search in query 'where' array
 *
 * @param array $where
 * @param string $field_name
 * @return string
 */
function _harvard_pnl_get_field_argument($where, $field_name) {
  foreach ($where as $key => $value) {
    if (!empty($value['field']) && $value['field'] == $field_name) {
      if (!is_array($value['value'])) {
        return $value['value'];
      }
      else {
        reset($value['value']);
        return current($value['value']);
      }
    }
    if (!empty($value['conditions'])) {
      $result = _harvard_pnl_get_field_argument($value['conditions'], $field_name);
      if (isset($result)) {
        return $result;
      }
    }
  }
  return NULL;
}