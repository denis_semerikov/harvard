<?php
/**
 * @file
 * harvard_pnl.theme.inc
 */

/**
 * Implementation hook_theme().
 */
function harvard_pnl_theme(&$existing, $type, $theme, $path) {
  $hooks = array();
  
  $hooks['coming_soon_page'] = array();
  $hooks['browser_by_last_name_links'] = array();
  $hooks['new_search_link'] = array(
    'variables' => array('url' => NULL),
  );

  return $hooks;
}


/**
 * Theme function for new search link
 * 
 * @param type $vars
 * 
 * @return string 
 *    Html of search link with wrapper
 */
function theme_new_search_link($vars) {
  $output = '';
  if (!empty($vars['url'])) {
    $output .= '<div class="new-search-link">';
    $output .= l(t('Start A New Search'), $vars['url']);
    $output .= '</div>';
  }
  return $output;
}
/**
 *
 * @param array $form
 * @param array $form_state 
 */
function theme_browser_by_last_name_links() {
  $args = arg();
  $alphas = range('A', 'Z');
  $output = '';
  foreach ($alphas as $char) {
    $class = '';
    if (isset($args[1])) {
      $class = (strtolower($args[1]) == strtolower($char)) ? 'active' : '';
    }
    $output .= l($char, PEOPLE_AND_LABS_SEARCH_URL . '/' . $char, array('attributes' => array('class' => array('harvard-people-labs-search-abc-button', $class))));
  }
  return $output;
}

/**
 *
 * @param array $form
 * @param array $form_state 
 */
function theme_coming_soon_page() {
  $output = t('This page will be open soon');
  return $output;
}