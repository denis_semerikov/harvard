<?php

/**
 * @file
 * harvard_pnl.rules.inc
 */

/**
 * Implements hook_rules_file_info().
 */
function harvard_pnl_rules_file_info() {
  $items = array();
  $items[] = 'harvard_pnl.rules';

  return $items;
}

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function harvard_pnl_rules_event_info() {
  $events = array();
  $events['harvard_pnl_rules_event_autorized_in_hc'] = array(
    'label' => t('After new drupal user autorization in Harvard auth.'),
    'group' => t('User'),
    'module' => 'harvard_pnl',
    'variables' => array(
      'account' => array('type' => 'user', 'label' => t('The user account that was autorized.')),
    ),
  );

  $events['harvard_pnl_rules_user_login_from_hc'] = array(
    'label' => t('After existing drupal user autorization in Harvard auth.'),
    'group' => t('User'),
    'module' => 'harvard_pnl',
    'variables' => array(
      'account' => array('type' => 'user', 'label' => t('The user account that was autorized.')),
    ),
  );

  $events['harvard_pnl_rules_user_selected_role'] = array(
    'label' => t('After new user role selecting (faculty/trainee).'),
    'group' => t('User'),
    'module' => 'harvard_pnl',
    'variables' => array(
      'account' => array('type' => 'user', 'label' => t('The user account that was autorized.')),
    ),
  );

  $events['harvard_pnl_rules_user_edit_doesnt_exist_in_hc'] = array(
    'label' => t('After editing user. User doesn\'t exist in HC profiles.'),
    'group' => t('User'),
    'module' => 'harvard_pnl',
    'variables' => array(
      'account' => array('type' => 'user', 'label' => t('The user account that was edited.')),
    ),
  );

  $events['harvard_pnl_rules_cron_user_update_started'] = array(
    'label' => t('After starting user update by cron from Harvard profiles.'),
    'group' => t('User'),
    'module' => 'harvard_pnl',
    'variables' => array(
      'account' => array('type' => 'user', 'label' => t('The user account that will be updated.')),
    ),
  );
  $events['harvard_pnl_rules_event_cron_user_anual_update_time_coming'] = array(
    'label' => t('After coming anual update time.'),
    'group' => t('User'),
    'module' => 'harvard_pnl',
    'variables' => array(
      'account' => array('type' => 'user', 'label' => t('The user account that will be updated.')),
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_condition_info().
 */
function harvard_pnl_rules_condition_info() {
  $conditions = array();

  $conditions['harvard_pnl_rules_condition_user_has_been_approved'] = array(
    'group' => t('User'),
    'label' => t('User have been approved'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_user_has_been_rejected'] = array(
    'group' => t('User'),
    'label' => t('User have been rejected'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_has_been_cnanged_supervising'] = array(
    'group' => t('User'),
    'label' => t('Have been changed Supervising Faculty Member'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_has_been_cnanged_expiry_date'] = array(
    'group' => t('User'),
    'label' => t('Have been changed Expiry date'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );


  $conditions['harvard_pnl_rules_condition_has_been_set_use_hc_profiles'] = array(
    'group' => t('User'),
    'label' => t('Have been set Use HC Profiles'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );


  $conditions['harvard_pnl_rules_condition_is_current_user_profile_updated'] = array(
    'group' => t('User'),
    'label' => t('Current user profile have been updated'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      )
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_new_user_register_request'] = array(
    'group' => t('User'),
    'label' => t('New user register request'),
    'parameter' => array(
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_user_rejected_status'] = array(
    'group' => t('User'),
    'label' => t('User have rejected status'),
    'parameter' => array(
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_user_approved_status'] = array(
    'group' => t('User'),
    'label' => t('User have approved status'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

  $conditions['harvard_pnl_rules_condition_has_been_cnanged_email'] = array(
    'group' => t('User'),
    'label' => t('Have been changed Email'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('unchanged user'),
        'handler' => 'rules_events_entity_unchanged',
      ),
    ),
    'module' => 'harvard_pnl',
  );

   $conditions['harvard_pnl_rules_condition_cron_annual_update_check_sended_email_flag'] = array(
      'group' => t('User'),
      'label' => t('If sended email flag = %value%.'),
      'parameter' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User'),
        ),
        'value' => array(
          'type' => 'integer',
          'label' => t('Comparsion value'),
        ),
      ),
      'module' => 'harvard_pnl',
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info() on behalf of the workbench_moderation module.
 */
function harvard_pnl_rules_action_info() {
  $actions = array();



  $actions['harvard_pnl_mail'] = array(
    'label' => t('Send mail (using harvard templates)'),
    'group' => t('System'),
    'parameter' => array(
      'to' => array(
        'type' => 'text',
        'label' => t('To'),
        'description' => t('The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822.'),
      ),
      'subject' => array(
        'type' => 'text',
        'label' => t('Subject'),
        'description' => t("The mail's subject."),
        'translatable' => TRUE,
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message'),
        'description' => t("The mail's message body."),
        'translatable' => TRUE,
      ),
      'from' => array(
        'type' => 'text',
        'label' => t('From'),
        'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
        'optional' => TRUE,
      ),
      'language' => array(
        'type' => 'token',
        'label' => t('Language'),
        'description' => t('If specified, the language used for getting the mail message and subject.'),
        'options list' => 'entity_metadata_language_list',
        'optional' => TRUE,
        'default value' => LANGUAGE_NONE,
        'default mode' => 'selector',
      ),
    ),
    'base' => 'harvard_pnl_mail',
    'access callback' => 'rules_system_integration_access',
  );

  $actions['harvard_pnl_mail_to_users_of_role'] = array(
    'label' => t('Send mail to all users of a role (using harvard templates)'),
    'group' => t('System'),
    'parameter' => array(
      'roles' => array(
        'type' => 'list<integer>',
        'label' => t('Roles'),
        'options list' => 'entity_metadata_user_roles',
        'description' => t('Select the roles whose users should receive the mail.'),
      ),
      'subject' => array(
        'type' => 'text',
        'label' => t('Subject'),
        'description' => t("The mail's subject."),
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message'),
        'description' => t("The mail's message body."),
      ),
      'from' => array(
        'type' => 'text',
        'label' => t('From'),
        'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
        'optional' => TRUE,
      ),
    ),
    'base' => 'harvard_pnl_mail_to_users_of_role',
    'access callback' => 'rules_system_integration_access',
  );


  $actions['harvard_pnl_user_set_update_date'] = array(
    'label' => t("Set Update Date for users"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_user_set_approve_date'] = array(
    'label' => t("Set Approve Date for users"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_user_set_user_status_to_pending'] = array(
    'label' => t("Set user status to Pending"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_user_set_user_status_to_rejected'] = array(
    'label' => t("Set user status to Rejected"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );
  $actions['harvard_pnl_user_set_user_status_to_rejected_and_save'] = array(
    'label' => t("Set user status to Rejected and save user"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_user_update_user_profile_from_hc'] = array(
    'label' => t("Update user from HC Profile"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_user_update_user_profile_from_hc_and_save'] = array(
    'label' => t("Update user from HC Profile and save"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_set_user_to_not_new'] = array(
    'label' => t("Set user status to NOT new user"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_set_user_name_as_email'] = array(
    'label' => t("Set user name as email"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  $actions['harvard_pnl_annual_update_set_email_sended_flag'] = array(
    'label' => t("Set number of email sended to user for annual update"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
      'value' => array(
        'type' => 'integer',
        'label' => t('Number of email, or 0 for reseting this flag'),
      ),
    ),
  );

  $actions['harvard_pnl_action_set_rejected_message'] = array(
    'label' => t("Set user rejected notes"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
      'value' => array(
        'type' => 'text',
        'label' => t('User rejected notes'),
      ),
    ),
  );

  $actions['harvard_pnl_reset_user_update_date_to_expiry_date'] = array(
    'label' => t("Reset user update date to expiry date"),
    'group' => t("User"),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
    ),
  );

  return $actions;
}

/**
 * Condition: Check if the status was changed to approved.
 *
 * @param $account
 *   A user object
 *
 * @param $account_unchanged
 *   A user object
 *
 * @return
 *   TRUE/FALSE depending on if the status was changed to approved.
 */
function harvard_pnl_rules_condition_user_has_been_approved($account, $account_unchanged) {
  return (
      isset($account->field_account_approve_status[LANGUAGE_NONE][0]['value']) &&
      isset($account_unchanged->field_account_approve_status[LANGUAGE_NONE][0]['value']) &&
      $account->field_account_approve_status[LANGUAGE_NONE][0]['value'] == 'approved' &&
      $account_unchanged->field_account_approve_status[LANGUAGE_NONE][0]['value'] != 'approved'
      ) ? TRUE : FALSE;
}

/**
 * Condition: Check if the status was changed to rejected.
 *
 * @param $account
 *   A user object
 *
 * @param $account_unchanged
 *   A user object
 *
 * @return
 *   TRUE/FALSE depending on if the status was changed to rejected.
 */
function harvard_pnl_rules_condition_user_has_been_rejected($account, $account_unchanged) {
  return (
      isset($account->field_account_approve_status[LANGUAGE_NONE][0]['value']) &&
      isset($account_unchanged->field_account_approve_status[LANGUAGE_NONE][0]['value']) &&
      $account->field_account_approve_status[LANGUAGE_NONE][0]['value'] == 'rejected' &&
      $account_unchanged->field_account_approve_status[LANGUAGE_NONE][0]['value'] != 'rejected'
      ) ? TRUE : FALSE;
}

/**
 * Condition: Check if the Supervising Faculty Member was changed.
 *
 * @param $account
 *   A user object
 *
 * @param $account_unchanged
 *   A user object
 *
 * @return
 *   TRUE/FALSE depending on if the Supervising Faculty Member was changed.
 */
function harvard_pnl_rules_condition_has_been_cnanged_supervising($account, $account_unchanged) {
  return (
      isset($account->field_account_faculty_member[LANGUAGE_NONE][0]['uid']) &&
      isset($account_unchanged->field_account_faculty_member[LANGUAGE_NONE][0]['uid']) &&
      $account->field_account_faculty_member[LANGUAGE_NONE][0]['uid'] != $account_unchanged->field_account_faculty_member[LANGUAGE_NONE][0]['uid']
      ) ? TRUE : FALSE;
}

/**
 * Condition: Check if the Expiry date was changed.
 *
 * @param $account
 *   A user object
 *
 * @param $account_unchanged
 *   A user object
 *
 * @return
 *   TRUE/FALSE depending on if the Expity date was changed.
 */
function harvard_pnl_rules_condition_has_been_cnanged_expiry_date($account, $account_unchanged) {
  return (
      isset($account->field_account_expiry_date[LANGUAGE_NONE][0]['value']) &&
      isset($account_unchanged->field_account_expiry_date[LANGUAGE_NONE][0]['value']) &&
      $account->field_account_expiry_date[LANGUAGE_NONE][0]['value'] != $account_unchanged->field_account_expiry_date[LANGUAGE_NONE][0]['value']
      ) ? TRUE : FALSE;
}

/**
 * Condition: Check if the Email was changed.
 *
 * @param $account
 *   A user object
 *
 * @param $account_unchanged
 *   A user object
 *
 * @return
 *   TRUE/FALSE depending on if the email was changed.
 */
function harvard_pnl_rules_condition_has_been_cnanged_email($account, $account_unchanged) {
  return (
      isset($account->mail) &&
      isset($account_unchanged->mail) &&
      $account->mail != $account_unchanged->mail
      ) ? TRUE : FALSE;
}

/**
 * Condition: Check if was checked user profile from HC.
 *
 * @param $account
 *   A user object
 *
 * @return
 *   TRUE/FALSE.
 */
function harvard_pnl_rules_condition_has_been_set_use_hc_profiles($account, $account_unchanged) {
  return (
      isset($account->field_account_hc_profile[LANGUAGE_NONE][0]['value']) &&
      isset($account_unchanged->field_account_hc_profile[LANGUAGE_NONE][0]['value']) &&
      empty($account_unchanged->field_account_hc_profile[LANGUAGE_NONE][0]['value']) &&
      !empty($account->field_account_hc_profile[LANGUAGE_NONE][0]['value'])
      ) ? TRUE : FALSE;
}

/**
 * Condition: Check if profile have been updated curent user.
 *
 * @param $account
 *   A user object
 *
 * @return
 *   TRUE/FALSE.
 */
function harvard_pnl_rules_condition_is_current_user_profile_updated($account) {
  global $user;
  return ($user->uid == $account->uid) ? TRUE : FALSE;
}

/**
 * Condition: if is new user.
 *
 * @param type $account
 *  A user object
 */
function harvard_pnl_rules_condition_new_user_register_request($account_unchanged) {
  return (
      isset($account_unchanged->field_account_is_new[LANGUAGE_NONE][0]['value']) &&
      !empty($account_unchanged->field_account_is_new[LANGUAGE_NONE][0]['value'])
      ) ? TRUE : FALSE;
}

/**
 * Condition: If status = rejected
 *
 * @param type $account
 *  A user object
 */
function harvard_pnl_rules_condition_user_rejected_status($account_unchanged) {
  return (
      isset($account_unchanged->field_account_approve_status[LANGUAGE_NONE][0]['value']) &&
      $account_unchanged->field_account_approve_status[LANGUAGE_NONE][0]['value'] == 'rejected'
      ) ? TRUE : FALSE;
}

/**
 * Condition: If status = approved
 *
 * @param type $account
 *  A user object
 */
function harvard_pnl_rules_condition_user_approved_status($account) {
  return (
      isset($account->field_account_approve_status[LANGUAGE_NONE][0]['value']) &&
      $account->field_account_approve_status[LANGUAGE_NONE][0]['value'] == 'approved'
      ) ? TRUE : FALSE;
}

/**
 * Condition: If sended email flag = %value%.
 *
 * @param type $account
 *  A user object
 * @param int $value
 *  Number of days that have been set in the rule condition
 */
function harvard_pnl_rules_condition_cron_annual_update_check_sended_email_flag($account, $value) {

  return (
      isset($account->field_annual_update_email_flag[LANGUAGE_NONE][0]['value']) &&
      $account->field_annual_update_email_flag[LANGUAGE_NONE][0]['value'] == $value ||
      !isset($account->field_annual_update_email_flag[LANGUAGE_NONE][0]['value']) &&
      $value == 0
      ) ? TRUE : FALSE;
}

/**
 * Action: Set update date for user.
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_set_update_date($account) {
  if (isset($account->field_account_update_date)) {
    $date = new DateTime();
    $account->field_account_update_date[LANGUAGE_NONE][0]['value'] = $date->format('Y-m-d H:i:s');
  }
}

/**
 * Action: Set approve date for user.
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_set_approve_date($account) {
  if (isset($account->field_account_date_of_approval)) {
    $date = new DateTime();
    $account->field_account_date_of_approval[LANGUAGE_NONE][0]['value'] = $date->format('Y-m-d H:i:s');
  }
}

/**
 * Action: Set user status to Pending
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_set_user_status_to_pending($account) {
  if (isset($account->field_account_approve_status)) {
    $account->field_account_approve_status[LANGUAGE_NONE][0]['value'] = 'pending';
  }
}

/**
 * Action: Set user status to Rejected
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_set_user_status_to_rejected($account) {
  if (isset($account->field_account_approve_status)) {
    $account->field_account_approve_status[LANGUAGE_NONE][0]['value'] = 'rejected';
  }
}

/**
 * Action: Set user status to Rejected and save
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_set_user_status_to_rejected_and_save($account) {


  if (isset($account->field_account_approve_status)) {
    $account->field_account_approve_status[LANGUAGE_NONE][0]['value'] = 'rejected';
  }
}

/**
 * Action: Update user from Harvard profiles
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_update_user_profile_from_hc($account) {
  if (isset($account->field_account_huid) && !empty($account->field_account_huid[LANGUAGE_NONE][0]['value'])) {
    $hc_profile_fields = harvard_pnl_hc_profiles_map();
    $hc_profile_data = harvard_pnl_get_hc_profile_data($account->field_account_huid[LANGUAGE_NONE][0]['value']);
    $hc_profile_map = harvard_pnl_hc_profile_drupal_map();
    if(!empty($hc_profile_data)){
      foreach ($hc_profile_data as $key => $value) {
        if (isset($hc_profile_map[$key]) && in_array($hc_profile_map[$key], $hc_profile_fields)) {
          $account->{$hc_profile_map[$key]}[LANGUAGE_NONE][0]['value'] = $value;
        }
      }
    }
    if (empty($hc_profile_data) && isset($account->field_account_hc_profile[LANGUAGE_NONE][0])) {
      // uncheck "Use HC Profiles for my contact info"
      $account->field_account_hc_profile[LANGUAGE_NONE][0]['value'] = 0;
      rules_invoke_event('harvard_pnl_rules_user_edit_doesnt_exist_in_hc', $account);
    }
  }
}

/**
 * Action: Update user from Harvard profiles and save
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_user_update_user_profile_from_hc_and_save($account) {
  if (isset($account->field_account_huid) &&
      !empty($account->field_account_huid[LANGUAGE_NONE][0]['value']) &&
      !empty($account->field_account_hc_profile[LANGUAGE_NONE][0]['value'])) {

    $hc_profile_fields = harvard_pnl_hc_profiles_map();
    $hc_profile_data = harvard_pnl_get_hc_profile_data($account->field_account_huid[LANGUAGE_NONE][0]['value']);
    $hc_profile_map = harvard_pnl_hc_profile_drupal_map();

    if(!empty($hc_profile_data)){
      foreach ($hc_profile_data as $key => $value) {
        if (isset($hc_profile_map[$key]) && in_array($hc_profile_map[$key], $hc_profile_fields)) {
          $account->{$hc_profile_map[$key]}[LANGUAGE_NONE][0]['value'] = $value;
        }
      }
    }
  }
}

/**
 * Action: Set user status to NOT new user
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_set_user_to_not_new($account) {
  if (isset($account->field_account_is_new) && !empty($account->field_account_is_new)) {
    $account->field_account_is_new[LANGUAGE_NONE][0]['value'] = 0;
  }
}

/**
 * Action: Set number of email sended to user for annual update.
 *
 * @param $account
 *   A user object
 * @param integer $value
 *   A number of sended email
 */
function harvard_pnl_annual_update_set_email_sended_flag($account, $value) {
  if (isset($account->field_annual_update_email_flag)) {
    $account->field_annual_update_email_flag[LANGUAGE_NONE][0]['value'] = $value;
  }
}

/**
 * Action: Set user rejected notes.
 *
 * @param $account
 *   A user object
 * @param string $value
 *   Rejected notes text
 */
function harvard_pnl_action_set_rejected_message($account, $value) {
  if (isset($account->field_account_rejected_notes)) {
    $account->field_account_rejected_notes[LANGUAGE_NONE][0]['value'] = $value;
  }
}

/**
 * Action: Set user status to Rejected
 *
 * $param $account
 *   A user object
 */
function harvard_pnl_set_user_name_as_email($account) {
  $account->name = $account->mail;
}

/**
 * Action: Reset user update date to expiry date
 *
 * @param $account
 *   A user object
 */
function harvard_pnl_reset_user_update_date_to_expiry_date($account) {

  if (isset($account->field_account_update_date) &&
      isset($account->field_account_expiry_date)) {

    $account->field_account_update_date[LANGUAGE_NONE][0]['value'] =
    date('Y-m-d H:i:s', strtotime($account->field_account_expiry_date[LANGUAGE_NONE][0]['value']) - ANNUAL_UPDATE_EXPIRY_INTERVAL + ANNUAL_UPDATE_START_NOTIFICATION);
  }
}

/**
 * Action Implementation: Send mail.
 */
function harvard_pnl_mail($to, $subject, $message, $from = NULL, $langcode, $settings, RulesState $state, RulesPlugin $element) {
  $to = str_replace(array("\r", "\n"), '', $to);
  $from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;
  $params = array(
    'subject' => harvard_pnl_replace_email_template($subject, 'subject', $state),
    'message' => harvard_pnl_replace_email_template($message, 'message', $state),
    'langcode' => $langcode,
  );
  // Set a unique key for this mail.
  $name = isset($element->root()->name) ? $element->root()->name : 'unnamed';
  $key = 'rules_action_mail_' . $name . '_' . $element->elementId();
  $languages = language_list();
  $language = $langcode == LANGUAGE_NONE ? language_default() : $languages[$langcode];

  $message = drupal_mail('rules', $key, $to, $language, $params, $from);
  if ($message['result']) {
    watchdog('rules', 'Successfully sent email to %recipient', array('%recipient' => $to));
  }
}

/**
 * Action: Send mail to all users of a specific role group(s).
 */
function harvard_pnl_mail_to_users_of_role($roles, $subject, $message, $from = NULL, $settings, RulesState $state, RulesPlugin $element) {
  $from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;

  // All authenticated users, which is everybody.
  if (in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
    $result = db_query('SELECT mail FROM {users} WHERE uid > 0');
  }
  else {
    $rids = implode(',', $roles);
    // Avoid sending emails to members of two or more target role groups.
    $result = db_query('SELECT DISTINCT u.mail FROM {users} u INNER JOIN {users_roles} r ON u.uid = r.uid WHERE r.rid IN (' . $rids . ')');
  }

  // Now, actually send the mails.
  $params = array(
    'subject' => harvard_pnl_replace_email_template($subject, 'subject', $state),
    'message' => harvard_pnl_replace_email_template($message, 'message', $state)
  );
  // Set a unique key for this mail.
  $name = isset($element->root()->name) ? $element->root()->name : 'unnamed';
  $key = 'rules_action_mail_to_users_of_role_' . $name . '_' . $element->elementId();
  $languages = language_list();

  $message = array('result' => TRUE);
  foreach ($result as $row) {
    $message = drupal_mail('rules', $key, $row->mail, language_default(), $params, $from);
    if (!$message['result']) {
      break;
    }
  }
  if ($message['result']) {
    $role_names = array_intersect_key(user_roles(TRUE), array_flip($roles));
    watchdog('rules', 'Successfully sent email to the role(s) %roles.', array('%roles' => implode(', ', $role_names)));
  }
}

function harvard_pnl_replace_email_template($text, $type, $state) {
  $templates_map = harvard_pnl_email_templates_map();
  $values = array();
  $templates = array();
  foreach ($templates_map as $key => $value) {
    $values[] = '[' . $key . '_' . $type . ']';
    $templates[] = variable_get('harvard_pnl_' . $key . '_' . $type, '');
  }
  $text = str_replace($values, $templates, $text);
  $text = harvard_pnl_token_evaluate($text, array(), $state);
  return $text;
}



/**  FROM RULES MODULE
   * We replace the tokens on our own as we cannot use token_replace(), because
   * token usually assumes that $data['node'] is a of type node, which doesn't
   * hold in general in our case.
   * So we properly map variable names to variable data types and then run the
   * replacement ourself.
   */
function harvard_pnl_token_evaluate($text, $options = array(), RulesState $state) {
    $var_info = $state->varInfo();
    $options += array('sanitize' => FALSE);

    $replacements = array();
    $data = array();
    // We also support replacing tokens in a list of textual values.
    $whole_text = is_array($text) ? implode('', $text) : $text;
    foreach (token_scan($whole_text) as $var_name => $tokens) {
      $var_name = str_replace('-', '_', $var_name);
      if (isset($var_info[$var_name]) && ($token_type = _rules_system_token_map_type($var_info[$var_name]['type']))) {
        // We have to key $data with the type token uses for the variable.
        $data = rules_unwrap_data(array($token_type => $state->get($var_name)), array($token_type => $var_info[$var_name]));
        $replacements += token_generate($token_type, $tokens, $data, $options);
      }
      else {
        $replacements += token_generate($var_name, $tokens, array(), $options);
      }
    }

    // Optionally clean the list of replacement values.
    if (!empty($options['callback']) && function_exists($options['callback'])) {
      $function = $options['callback'];
      $function($replacements, $data, $options);
    }

    // Actually apply the replacements.
    $tokens = array_keys($replacements);
    $values = array_values($replacements);
    if (is_array($text)) {
      foreach ($text as $i => $text_item) {
        $text[$i] = str_replace($tokens, $values, $text_item);
      }
      return $text;
    }
    return str_replace($tokens, $values, $text);
}
