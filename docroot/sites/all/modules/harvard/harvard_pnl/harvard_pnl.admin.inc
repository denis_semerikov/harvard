<?php

/**
 * @file
 * Describe configuration section for module
 */

/**
 * Create settings form in drupal configuration setings section for module
 */
function harvard_pnl_admin_email_settings() {
  $form = array();
  $templates = harvard_pnl_email_templates_map();

  foreach ($templates as $key => $template) {
    $form['harvard_pnl_' . $key] = array(
      '#type' => 'fieldset',
      '#title' => $template,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['harvard_pnl_' . $key]['harvard_pnl_' . $key . '_subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get('harvard_pnl_' . $key . '_subject', ''),
      '#required' => TRUE,
    );

    $form['harvard_pnl_' . $key]['harvard_pnl_' . $key . '_message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#default_value' => variable_get('harvard_pnl_' . $key . '_message', ''),
      '#required' => TRUE,
    );

    $form['harvard_pnl_' . $key]['harvard_pnl_' . $key . '_key'] = array(
      '#type' => 'item',
      '#title' => t('Keys'),
      '#markup' => '[' . $key . '_subject]<br>[' . $key . '_message]',
    );
  }


  $form['harvard_pnl_tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $patterns = harvard_pnl_get_replacement_patterns_for_rules_email();

  $form['harvard_pnl_tokens']['harvard_pnl_tokens_patterns'] = array(
    '#type' => 'item',
    '#title' => t('Available replacement patterns'),
    '#markup' => $patterns,
  );

  return system_settings_form($form, TRUE);
}

/**
 * Create settings form in drupal configuration setings section for module
 */
function harvard_pnl_admin_hc_profile_settings() {
  $form = array();

  $form['harvard_pnl_hc_profile_url'] = array(
    '#type' => 'textfield',
    '#title' => t('HC Profile API url'),
    '#default_value' => variable_get('harvard_pnl_hc_profile_url', 'http://connects.catalyst.harvard.edu/PROFILESAPI/ProfileService.svc/ProfileSearch'),
    '#required' => TRUE,
    '#size' => 100
  );


  return system_settings_form($form, TRUE);
}
