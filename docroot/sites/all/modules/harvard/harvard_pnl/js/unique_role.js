(function ($) {
  $(function () {
    var pnlFacultyRoleId = Drupal.settings.pnlFacultyRoleId;
    var pnlTraineeRoleId = Drupal.settings.pnlTraineeRoleId;
    var faculty_checkbox = $('input:checkbox[name="roles[' + pnlFacultyRoleId + ']"], input:checkbox[name="roles_change[' + pnlFacultyRoleId + ']"]');
    var trainee_checkbox = $('input:checkbox[name="roles[' + pnlTraineeRoleId + ']"], input:checkbox[name="roles_change[' + pnlTraineeRoleId + ']"]');

    // first load. if checket both - check only faculty
    if (faculty_checkbox.is(":checked")) {
      trainee_checkbox.removeAttr('checked');
    }

    faculty_checkbox.click(function(){
      if ($(this).is(":checked")) {
        trainee_checkbox.removeAttr('checked');
      }
    });

    trainee_checkbox.click(function(){
      if ($(this).is(":checked")) {
        faculty_checkbox.removeAttr('checked');
      }
    });

  });


})(jQuery);
