(function ($) {
  Drupal.behaviors.userProfileEdit = {
    attach: function (context, settings) {
      var hc_profile_checkbox = $("#edit-field-account-hc-profile-und");
      var hc_profile_fields = $(".hc-profile-fields input, input.hc-profile-fields, .hc-profile-fields textarea");
      var hc_required_labels = $('.hc-required label');

      if (!hc_profile_checkbox.is(":disabled")) {
        hc_set_status(hc_profile_checkbox, hc_profile_fields, hc_required_labels);
        hc_profile_checkbox.click(function(){
          hc_set_status(hc_profile_checkbox, hc_profile_fields, hc_required_labels);
            
          if($(this).attr('checked') == true){
            $(this).trigger('use_hc_checked');
          }
        });
      }
    }
  };

  function hc_set_status(hc_profile_checkbox, hc_profile_fields, hc_required_labels){
    if (hc_profile_checkbox.is(":checked")) {
      hc_required_labels.find('span.form-required').empty();
      hc_profile_fields.attr("disabled", "disabled");
      hc_profile_fields.css("disabled");
    } else {
      if(hc_required_labels.find('span.form-required').length < 1) {
        hc_required_labels.append('<span class="form-required" title="' + Drupal.t('This field is required.') + '">*</span>');
      }
      hc_profile_fields.removeAttr('disabled');
      hc_profile_fields.removeClass("disabled");
    }
  }
  
})(jQuery);  
