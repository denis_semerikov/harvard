<?php
/**
 * @file
 * events_feature.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function events_feature_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-events
  $menus['menu-events'] = array(
    'menu_name' => 'menu-events',
    'title' => 'Events',
    'description' => '',
  );
  // Exported menu: menu-header-menu
  $menus['menu-header-menu'] = array(
    'menu_name' => 'menu-header-menu',
    'title' => 'Header menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  t('Header menu');


  return $menus;
}
