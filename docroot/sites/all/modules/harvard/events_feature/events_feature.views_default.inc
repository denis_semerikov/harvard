<?php
/**
 * @file
 * events_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function events_feature_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Upcoming events';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '80';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 1;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Sponsor/Institution */
  $handler->display->display_options['fields']['field_events_sponsor_institution']['id'] = 'field_events_sponsor_institution';
  $handler->display->display_options['fields']['field_events_sponsor_institution']['table'] = 'field_data_field_events_sponsor_institution';
  $handler->display->display_options['fields']['field_events_sponsor_institution']['field'] = 'field_events_sponsor_institution';
  $handler->display->display_options['fields']['field_events_sponsor_institution']['label'] = '';
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_events_sponsor_institution']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_events_sponsor_institution']['field_api_classes'] = 0;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_events_date']['id'] = 'field_events_date';
  $handler->display->display_options['fields']['field_events_date']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['fields']['field_events_date']['field'] = 'field_events_date';
  $handler->display->display_options['fields']['field_events_date']['label'] = '';
  $handler->display->display_options['fields']['field_events_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_events_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_events_date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_events_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_events_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_events_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_events_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_events_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_events_date']['settings'] = array(
    'format_type' => 'event_date_format',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_events_date']['field_api_classes'] = 0;
  /* Field: Content: Location teaser */
  $handler->display->display_options['fields']['field_events_location_teaser']['id'] = 'field_events_location_teaser';
  $handler->display->display_options['fields']['field_events_location_teaser']['table'] = 'field_data_field_events_location_teaser';
  $handler->display->display_options['fields']['field_events_location_teaser']['field'] = 'field_events_location_teaser';
  $handler->display->display_options['fields']['field_events_location_teaser']['label'] = '';
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_events_location_teaser']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_events_location_teaser']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_events_location_teaser']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_events_location_teaser']['field_api_classes'] = 0;
  /* Sort criterion: Content: Date -  start date (field_events_date) */
  $handler->display->display_options['sorts']['field_events_date_value']['id'] = 'field_events_date_value';
  $handler->display->display_options['sorts']['field_events_date_value']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['sorts']['field_events_date_value']['field'] = 'field_events_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'events' => 'events',
  );

  /* Display: Upcoming events */
  $handler = $view->new_display('page', 'Upcoming events', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Upcoming Events';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'events' => 'events',
  );
  /* Filter criterion: Content: Date -  start date (field_events_date) */
  $handler->display->display_options['filters']['field_events_date_value']['id'] = 'field_events_date_value';
  $handler->display->display_options['filters']['field_events_date_value']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['filters']['field_events_date_value']['field'] = 'field_events_date_value';
  $handler->display->display_options['filters']['field_events_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_events_date_value']['granularity'] = 'minute';
  $handler->display->display_options['filters']['field_events_date_value']['default_date'] = 'now';
  /* Filter criterion: Content: Sponsor/Institution (field_events_sponsor_institution) */
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['id'] = 'field_events_sponsor_institution_target_id';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['table'] = 'field_data_field_events_sponsor_institution';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['field'] = 'field_events_sponsor_institution_target_id';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['operator_id'] = 'field_events_sponsor_institution_target_id_op';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['label'] = 'Filter by:';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['operator'] = 'field_events_sponsor_institution_target_id_op';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['identifier'] = 'field_events_sponsor_institution_target_id';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['reduce'] = 0;
  $handler->display->display_options['path'] = 'events';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Upcoming events';
  $handler->display->display_options['menu']['name'] = 'menu-events';

  /* Display: Past events */
  $handler = $view->new_display('page', 'Past events', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Past Events';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'events' => 'events',
  );
  /* Filter criterion: Content: Date -  start date (field_events_date) */
  $handler->display->display_options['filters']['field_events_date_value']['id'] = 'field_events_date_value';
  $handler->display->display_options['filters']['field_events_date_value']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['filters']['field_events_date_value']['field'] = 'field_events_date_value';
  $handler->display->display_options['filters']['field_events_date_value']['operator'] = '<';
  $handler->display->display_options['filters']['field_events_date_value']['granularity'] = 'minute';
  $handler->display->display_options['filters']['field_events_date_value']['default_date'] = 'now';
  /* Filter criterion: Content: Sponsor/Institution (field_events_sponsor_institution) */
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['id'] = 'field_events_sponsor_institution_target_id';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['table'] = 'field_data_field_events_sponsor_institution';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['field'] = 'field_events_sponsor_institution_target_id';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['operator_id'] = 'field_events_sponsor_institution_target_id_op';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['label'] = 'Filter by:';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['operator'] = 'field_events_sponsor_institution_target_id_op';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['identifier'] = 'field_events_sponsor_institution_target_id';
  $handler->display->display_options['filters']['field_events_sponsor_institution_target_id']['expose']['reduce'] = 0;
  $handler->display->display_options['path'] = 'past-events';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Past events';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'menu-events';
  $handler->display->display_options['menu']['context'] = 1;

  /* Display: Home page events block */
  $handler = $view->new_display('block', 'Home page events block', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['empty'] = FALSE;
  $handler->display->display_options['footer']['area']['content'] = '<a href="/events">View more events</a>';
  $handler->display->display_options['footer']['area']['format'] = 'filtered_html';
  $handler->display->display_options['footer']['area']['tokenize'] = 0;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_events_location']['id'] = 'field_events_location';
  $handler->display->display_options['fields']['field_events_location']['table'] = 'field_data_field_events_location';
  $handler->display->display_options['fields']['field_events_location']['field'] = 'field_events_location';
  $handler->display->display_options['fields']['field_events_location']['label'] = '';
  $handler->display->display_options['fields']['field_events_location']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_events_location']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_events_location']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_events_location']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_events_location']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_events_location']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_events_location']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_events_location']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_events_location']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_events_location']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_events_location']['field_api_classes'] = 0;
  /* Field: Date Month */
  $handler->display->display_options['fields']['field_events_date']['id'] = 'field_events_date';
  $handler->display->display_options['fields']['field_events_date']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['fields']['field_events_date']['field'] = 'field_events_date';
  $handler->display->display_options['fields']['field_events_date']['ui_name'] = 'Date Month';
  $handler->display->display_options['fields']['field_events_date']['label'] = '';
  $handler->display->display_options['fields']['field_events_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_events_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_events_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_events_date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_events_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_events_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_events_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_events_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_events_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_events_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_events_date']['settings'] = array(
    'format_type' => 'month',
    'fromto' => 'value',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_events_date']['field_api_classes'] = 0;
  /* Field: Date day */
  $handler->display->display_options['fields']['field_events_date_1']['id'] = 'field_events_date_1';
  $handler->display->display_options['fields']['field_events_date_1']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['fields']['field_events_date_1']['field'] = 'field_events_date';
  $handler->display->display_options['fields']['field_events_date_1']['ui_name'] = 'Date day';
  $handler->display->display_options['fields']['field_events_date_1']['label'] = '';
  $handler->display->display_options['fields']['field_events_date_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_events_date_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_events_date_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_events_date_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_events_date_1']['settings'] = array(
    'format_type' => 'day',
    'fromto' => 'value',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_events_date_1']['field_api_classes'] = 0;
  /* Field: Date wrapper */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Date wrapper';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="month">[field_events_date]</div>
<div class="day">[field_events_date_1]</div>';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '80';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 1;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'events' => 'events',
  );
  /* Filter criterion: Content: Date -  start date (field_events_date) */
  $handler->display->display_options['filters']['field_events_date_value']['id'] = 'field_events_date_value';
  $handler->display->display_options['filters']['field_events_date_value']['table'] = 'field_data_field_events_date';
  $handler->display->display_options['filters']['field_events_date_value']['field'] = 'field_events_date_value';
  $handler->display->display_options['filters']['field_events_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_events_date_value']['granularity'] = 'minute';
  $handler->display->display_options['filters']['field_events_date_value']['default_date'] = 'now';
  $handler->display->display_options['block_description'] = 'Home page events block';
  $export['events'] = $view;

  return $export;
}
