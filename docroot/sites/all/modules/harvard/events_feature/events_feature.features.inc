<?php
/**
 * @file
 * events_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function events_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function events_feature_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function events_feature_node_info() {
  $items = array(
    'events' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Events content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
