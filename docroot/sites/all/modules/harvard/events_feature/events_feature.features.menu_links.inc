<?php
/**
 * @file
 * events_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function events_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-events:events
  $menu_links['menu-events:events'] = array(
    'menu_name' => 'menu-events',
    'link_path' => 'events',
    'router_path' => 'events',
    'link_title' => 'Upcoming Events',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: menu-events:past-events
  $menu_links['menu-events:past-events'] = array(
    'menu_name' => 'menu-events',
    'link_path' => 'past-events',
    'router_path' => 'past-events',
    'link_title' => 'Past Events',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: menu-header-menu:events
  $menu_links['menu-header-menu:events'] = array(
    'menu_name' => 'menu-header-menu',
    'link_path' => 'events',
    'router_path' => 'events',
    'link_title' => 'Events',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  t('Past Events');
  t('Upcoming Events');


  return $menu_links;
}
