<?php
/**
 * @file
 * events_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function events_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'event_context';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'events' => 'events',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'events:page' => 'events:page',
        'events:page_1' => 'events:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-events' => array(
          'module' => 'menu',
          'delta' => 'menu-events',
          'region' => 'sidebar_first',
          'weight' => '-24',
        ),
        'webform-client-block-864' => array(
          'module' => 'webform',
          'delta' => 'client-block-864',
          'region' => 'sidebar_first',
          'weight' => '20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['event_context'] = $context;

  return $export;
}
