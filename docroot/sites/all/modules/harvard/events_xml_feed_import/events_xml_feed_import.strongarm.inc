<?php
/**
 * @file
 * events_xml_feed_import.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function events_xml_feed_import_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_default_timezone';
  $strongarm->value = 'America/New_York';
  $export['date_default_timezone'] = $strongarm;

  return $export;
}
