<?php
/**
 * @file
 * events_xml_feed_import.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function events_xml_feed_import_default_rules_configuration() {
  $items = array();
  $items['rules_events_publisher_on_import'] = entity_import('rules_config', '{ "rules_events_publisher_on_import" : {
      "LABEL" : "events publisher on import",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "events" ],
      "REQUIRES" : [ "rules", "workbench_moderation" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "events" : "events" } } } },
        { "data_is" : { "data" : [ "node:log" ], "value" : "Created by FeedsNodeProcessor" } }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "All imported items are published", "repeat" : 0 } },
        { "workbench_moderation_set_state" : {
            "node" : [ "node" ],
            "moderation_state" : "published",
            "force_transition" : 1
          }
        }
      ]
    }
  }');
  return $items;
}
