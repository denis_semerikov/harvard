<?php
/**
 * @file
 * site_rss_feeds.features.inc
 */

/**
 * Implements hook_views_api().
 */
function site_rss_feeds_views_api() {
  return array("version" => "3.0");
}
