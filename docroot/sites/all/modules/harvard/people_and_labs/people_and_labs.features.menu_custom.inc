<?php
/**
 * @file
 * people_and_labs.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function people_and_labs_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-header-menu-logout
  $menus['menu-header-menu-logout'] = array(
    'menu_name' => 'menu-header-menu-logout',
    'title' => 'Header menu logout',
    'description' => '',
  );
  // Exported menu: menu-people-labs
  $menus['menu-people-labs'] = array(
    'menu_name' => 'menu-people-labs',
    'title' => 'People & Labs',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Header menu logout');
  t('Main menu');
  t('People & Labs');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
