<?php
/**
 * @file
 * people_and_labs.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function people_and_labs_default_rules_configuration() {
  $items = array();
  $items['rules_annual_update_reject_expired_trainee_account_'] = entity_import('rules_config', '{ "rules_annual_update_reject_expired_trainee_account_" : {
      "LABEL" : "Annual update: Reject expired Trainee account.",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "annual update", "cron" ],
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_event_cron_user_anual_update_time_coming" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5" } },
            "operation" : "OR"
          }
        },
        { "harvard_pnl_rules_condition_user_approved_status" : { "account" : [ "account" ] } },
        { "OR" : [
            { "data_is" : {
                "data" : [ "account:field-account-expiry-date" ],
                "op" : "\\u003C",
                "value" : "now"
              }
            },
            { "data_is" : { "data" : [ "account:field-account-expiry-date" ], "value" : "now" } }
          ]
        }
      ],
      "DO" : [
        { "harvard_pnl_user_set_user_status_to_rejected" : { "account" : [ "account" ] } },
        { "harvard_pnl_action_set_rejected_message" : {
            "account" : [ "account" ],
            "value" : "Annual update: user has been rejected by expiration date."
          }
        },
        { "drupal_message" : { "message" : "User rejected by expiration date", "repeat" : 0 } },
        { "harvard_pnl_mail" : {
            "to" : [ "account:mail" ],
            "subject" : "[annual_update_email_to_trainee_he_has_been_rejected_subject]",
            "message" : "[annual_update_email_to_trainee_he_has_been_rejected_message] ",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        },
        { "harvard_pnl_mail" : {
            "to" : [ "account:field-account-faculty-member:mail" ],
            "subject" : "[annual_update_email_to_advisor_trainee_has_been_rejected_subject]",
            "message" : "[annual_update_email_to_advisor_trainee_has_been_rejected_message] ",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_annual_update_reject_user_account_update_date_expired'] = entity_import('rules_config', '{ "rules_annual_update_reject_user_account_update_date_expired" : {
      "LABEL" : "Annual update: Reject user account, update date expired",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "annual update", "cron" ],
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_event_cron_user_anual_update_time_coming" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        },
        { "harvard_pnl_rules_condition_user_approved_status" : { "account" : [ "account" ] } },
        { "data_is" : {
            "data" : [ "account:field-account-update-date" ],
            "op" : "\\u003C",
            "value" : "-1year"
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_user_set_user_status_to_rejected" : { "account" : [ "account" ] } },
        { "harvard_pnl_action_set_rejected_message" : {
            "account" : [ "account" ],
            "value" : "Annual update: user has been rejected (expired update time)"
          }
        },
        { "drupal_message" : {
            "message" : "User  [account:uid] has been blocked by update time epiry",
            "repeat" : 0
          }
        }
      ]
    }
  }');
  $items['rules_annual_update_reset_trainee_update_date'] = entity_import('rules_config', '{ "rules_annual_update_reset_trainee_update_date" : {
      "LABEL" : "Annual update: Reset Trainee update date",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "10",
      "TAGS" : [ "annual update", "cron" ],
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_event_cron_user_anual_update_time_coming" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5" } },
            "operation" : "OR"
          }
        },
        { "data_is" : {
            "data" : [ "account:field-account-expiry-date" ],
            "op" : "\\u003E",
            "value" : { "select" : "site:current-date", "date_offset" : { "value" : 7776000 } }
          }
        },
        { "harvard_pnl_rules_condition_user_approved_status" : { "account" : [ "account" ] } }
      ],
      "DO" : [
        { "harvard_pnl_reset_user_update_date_to_expiry_date" : { "account" : [ "account" ] } },
        { "drupal_message" : {
            "message" : "Reset trainee user [account:uid] update date to expire date",
            "repeat" : 0
          }
        }
      ]
    }
  }');
  $items['rules_annual_update_send_email_14_days_to_blocking'] = entity_import('rules_config', '{ "rules_annual_update_send_email_14_days_to_blocking" : {
      "LABEL" : "Annual update: Send email  - 14 days to blocking",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "annual update", "cron" ],
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_event_cron_user_anual_update_time_coming" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        },
        { "harvard_pnl_rules_condition_user_approved_status" : { "account" : [ "account" ] } },
        { "NOT harvard_pnl_rules_condition_cron_annual_update_check_sended_email_flag" : { "account" : [ "account" ], "value" : "1" } },
        { "data_is" : {
            "data" : [ "account:field-account-update-date" ],
            "op" : "\\u003C",
            "value" : "-1year +14day"
          }
        },
        { "data_is" : {
            "data" : [ "account:field-account-update-date" ],
            "op" : "\\u003E",
            "value" : "-1year+7day"
          }
        }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "Email notification sent to user [account:uid], 14 days to accout blocking",
            "repeat" : 0
          }
        },
        { "harvard_pnl_annual_update_set_email_sended_flag" : { "account" : [ "account" ], "value" : "1" } },
        { "harvard_pnl_mail" : {
            "to" : [ "account:mail" ],
            "subject" : "[annual_update_send_raminder_subject]",
            "message" : "[annual_update_send_raminder_message] ",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_annual_update_send_email_7_days_to_blocking'] = entity_import('rules_config', '{ "rules_annual_update_send_email_7_days_to_blocking" : {
      "LABEL" : "Annual update: Send email  - 7 days to blocking",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "annual update", "cron" ],
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_event_cron_user_anual_update_time_coming" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        },
        { "harvard_pnl_rules_condition_user_approved_status" : { "account" : [ "account" ] } },
        { "NOT harvard_pnl_rules_condition_cron_annual_update_check_sended_email_flag" : { "account" : [ "account" ], "value" : "2" } },
        { "data_is" : {
            "data" : [ "account:field-account-update-date" ],
            "op" : "\\u003C",
            "value" : "-1year+7day"
          }
        },
        { "data_is" : {
            "data" : [ "account:field-account-update-date" ],
            "op" : "\\u003E",
            "value" : "-1year"
          }
        }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "Email notification sent to user [account:uid], 7 days to accout blocking",
            "repeat" : 0
          }
        },
        { "harvard_pnl_annual_update_set_email_sended_flag" : { "account" : [ "account" ], "value" : "2" } },
        { "harvard_pnl_mail" : {
            "to" : [ "account:mail" ],
            "subject" : "[annual_update_send_raminder_subject]",
            "message" : "[annual_update_send_raminder_message]",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_approve'] = entity_import('rules_config', '{ "rules_approve" : {
      "LABEL" : "User has been approved",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "harvard_pnl", "rules" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "harvard_pnl_rules_condition_user_has_been_approved" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        },
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "User has been approved ([account:name])" } },
        { "harvard_pnl_user_set_update_date" : { "account" : [ "account" ] } },
        { "harvard_pnl_user_set_approve_date" : { "account" : [ "account" ] } },
        { "harvard_pnl_annual_update_set_email_sended_flag" : { "account" : [ "account" ], "value" : "0" } }
      ]
    }
  }');
  $items['rules_create_new_user_form'] = entity_import('rules_config', '{ "rules_create_new_user_form" : {
      "LABEL" : "Create new user (Admin or Editor)",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_insert" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        },
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "3" : "3", "9" : "9" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "[account:edit-url]" } } ]
    }
  }');
  $items['rules_cron_user_updating_from_hc_profiles'] = entity_import('rules_config', '{ "rules_cron_user_updating_from_hc_profiles" : {
      "LABEL" : "Cron: User updating from HC profiles",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "cron" ],
      "REQUIRES" : [ "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_cron_user_update_started" ],
      "DO" : [
        { "harvard_pnl_user_update_user_profile_from_hc_and_save" : { "account" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_edit_user_doesn_t_exist_in_hc_profile'] = entity_import('rules_config', '{ "rules_edit_user_doesn_t_exist_in_hc_profile" : {
      "LABEL" : "Trainee user doesn\\u0027t exist in HC Profile",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_user_edit_doesnt_exist_in_hc" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3", "9" : "9" } },
            "subject" : " [trainee_user_not_in_hc_profiles_subject]",
            "message" : "[trainee_user_not_in_hc_profiles_message] ",
            "from" : "[site:mail]"
          }
        },
        { "drupal_message" : {
            "message" : "We were not able to locate you in the HMS database.",
            "type" : "warning"
          }
        }
      ]
    }
  }');
  $items['rules_email_have_been_changed'] = entity_import('rules_config', '{ "rules_email_have_been_changed" : {
      "LABEL" : "Email have been changed",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "harvard_pnl", "rules" ],
      "ON" : [ "user_update" ],
      "IF" : [
        { "harvard_pnl_rules_condition_has_been_cnanged_email" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        },
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } }
      ],
      "DO" : [
        { "harvard_pnl_set_user_name_as_email" : { "account" : [ "account" ] } },
        { "drupal_message" : { "message" : "Email have been changed by user\\r\\nName have been changed to email" } }
      ]
    }
  }');
  $items['rules_faculty_user_doesn_t_exist_in_hc_profile'] = entity_import('rules_config', '{ "rules_faculty_user_doesn_t_exist_in_hc_profile" : {
      "LABEL" : "Faculty User doesn\\u0027t exist in HC Profile",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_user_edit_doesnt_exist_in_hc" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "6" : "6" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3", "9" : "9" } },
            "subject" : " [faculty_user_not_in_hc_profiles_subject]",
            "message" : "[faculty_user_not_in_hc_profiles_message] ",
            "from" : "[site:mail]"
          }
        },
        { "drupal_message" : {
            "message" : "We were not able to locate you in the HMS database. Please enter First, Last, Institution, email. Someone will contact you.",
            "type" : "warning"
          }
        }
      ]
    }
  }');
  $items['rules_faculty_user_updated_profile'] = entity_import('rules_config', '{ "rules_faculty_user_updated_profile" : {
      "LABEL" : "Faculty user updated profile",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "6" : "6" } } } },
        { "harvard_pnl_rules_condition_is_current_user_profile_updated" : { "account" : [ "account" ] } }
      ],
      "DO" : [
        { "harvard_pnl_user_set_update_date" : { "account" : [ "account" ] } },
        { "drupal_message" : { "message" : "Page Updated \\r\\nFaculty member" } }
      ]
    }
  }');
  $items['rules_need_approve'] = entity_import('rules_config', '{ "rules_need_approve" : {
      "LABEL" : "Need Faculty member approve",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_update" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "6" : "6" } } } },
        { "harvard_pnl_rules_condition_new_user_register_request" : { "account_unchanged" : [ "account_unchanged" ] } },
        { "NOT harvard_pnl_rules_condition_user_has_been_approved" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_mail_to_users_of_role" : {
            "roles" : { "value" : { "9" : "9" } },
            "subject" : "[approve_to_editor_subject]",
            "message" : "[approve_to_editor_message] ",
            "from" : "[site:mail]"
          }
        },
        { "drupal_message" : { "message" : "New user (Faculty) register request ([account:name])" } }
      ]
    }
  }');
  $items['rules_need_trainee_member_approve'] = entity_import('rules_config', '{ "rules_need_trainee_member_approve" : {
      "LABEL" : "Need Trainee member approve",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_update" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } },
        { "harvard_pnl_rules_condition_new_user_register_request" : { "account_unchanged" : [ "account_unchanged" ] } },
        { "NOT harvard_pnl_rules_condition_user_has_been_approved" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_mail" : {
            "to" : [ "account:field-account-faculty-member:mail" ],
            "subject" : "[approve_to_faculty_subject]",
            "message" : "[approve_to_faculty_message] ",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        },
        { "drupal_message" : { "message" : "New user (Tranee) register request ([account:name]) !!!" } }
      ]
    }
  }');
  $items['rules_redirect_to_home_page_on_user_login'] = entity_import('rules_config', '{ "rules_redirect_to_home_page_on_user_login" : {
      "LABEL" : "system_ Redirect existing user to his member page after HC auth",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "system" ],
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_user_login_from_hc" ],
      "DO" : [ { "redirect" : { "url" : "user" } } ]
    }
  }');
  $items['rules_rejected_to_pending'] = entity_import('rules_config', '{ "rules_rejected_to_pending" : {
      "LABEL" : "Rejected to Pending",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "harvard_pnl", "rules" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "harvard_pnl_rules_condition_user_rejected_status" : { "account_unchanged" : [ "account_unchanged" ] } },
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_user_set_user_status_to_pending" : { "account" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_set_pending_status_for_new_users'] = entity_import('rules_config', '{ "rules_set_pending_status_for_new_users" : {
      "LABEL" : "Set pending status for new users",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        },
        { "harvard_pnl_rules_condition_new_user_register_request" : { "account_unchanged" : [ "account_unchanged" ] } },
        { "NOT harvard_pnl_rules_condition_user_has_been_approved" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Set pending status for new user" } },
        { "harvard_pnl_user_set_user_status_to_pending" : { "account" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_trainee_logged_pending_status_'] = entity_import('rules_config', '{ "rules_trainee_logged_pending_status_" : {
      "LABEL" : "Trainee logged (pending status)",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_login" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } },
        { "NOT harvard_pnl_rules_condition_user_approved_status" : [] }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Your page is inactive and must be approved by your advisor" } }
      ]
    }
  }');
  $items['rules_user__authrorized_in_hc'] = entity_import('rules_config', '{ "rules_user__authrorized_in_hc" : {
      "LABEL" : "Authrorized in HC",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "harvard_pnl", "rules" ],
      "ON" : [ "harvard_pnl_rules_event_autorized_in_hc" ],
      "DO" : [
        { "harvard_pnl_user_update_user_profile_from_hc" : { "account" : [ "site:current-user" ] } },
        { "redirect" : { "url" : "user\\/[site:current-user:uid]\\/edit", "force" : 0 } }
      ]
    }
  }');
  $items['rules_user_changed_status_from_new_to_ordinary'] = entity_import('rules_config', '{ "rules_user_changed_status_from_new_to_ordinary" : {
      "LABEL" : "New user request from",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "harvard_pnl", "rules" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "harvard_pnl_rules_condition_new_user_register_request" : { "account_unchanged" : [ "account_unchanged" ] } },
        { "NOT user_has_role" : {
            "account" : [ "account-unchanged" ],
            "roles" : { "value" : { "10" : "10" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "User status \\u0022is new\\u0022 has been changed to ordinary account." } },
        { "harvard_pnl_set_user_to_not_new" : { "account" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_user_has_been_rejected'] = entity_import('rules_config', '{ "rules_user_has_been_rejected" : {
      "LABEL" : "User has been rejected",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "harvard_pnl", "rules" ],
      "ON" : [ "user_update" ],
      "IF" : [
        { "harvard_pnl_rules_condition_user_has_been_rejected" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        },
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_mail" : {
            "to" : "[account:mail]",
            "subject" : " [user_has_been_rejected_subject]",
            "message" : "[user_has_been_rejected_message] ",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        },
        { "drupal_message" : { "message" : [ "account:field-account-rejected-notes" ] } }
      ]
    }
  }');
  $items['rules_user_has_been_updated'] = entity_import('rules_config', '{ "rules_user_has_been_updated" : {
      "LABEL" : "Trainee user updated profile (was changed Supervising Faculty Member or Expiry Date )",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } },
        { "harvard_pnl_rules_condition_is_current_user_profile_updated" : { "account" : [ "account" ] } },
        { "OR" : [
            { "harvard_pnl_rules_condition_has_been_cnanged_supervising" : {
                "account" : [ "account" ],
                "account_unchanged" : [ "account_unchanged" ]
              }
            },
            { "harvard_pnl_rules_condition_has_been_cnanged_expiry_date" : {
                "account" : [ "account" ],
                "account_unchanged" : [ "account_unchanged" ]
              }
            }
          ]
        }
      ],
      "DO" : [
        { "harvard_pnl_user_set_user_status_to_pending" : { "account" : [ "account" ] } },
        { "drupal_message" : { "message" : "Needs approval\\r\\nTrainee member update profile (was changed Supervising Faculty Member or Expiry date)" } }
      ]
    }
  }');
  $items['rules_user_profile_update_from_hc_profile'] = entity_import('rules_config', '{ "rules_user_profile_update_from_hc_profile" : {
      "LABEL" : "Faculty user profile update from HC profile",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "6" : "6" } },
            "operation" : "OR"
          }
        },
        { "harvard_pnl_rules_condition_has_been_set_use_hc_profiles" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        }
      ],
      "DO" : [
        { "harvard_pnl_user_update_user_profile_from_hc" : { "account" : [ "account" ] } },
        { "drupal_message" : { "message" : "User updated from HC profile" } }
      ]
    }
  }');
  $items['rules_user_role_selected'] = entity_import('rules_config', '{ "rules_user_role_selected" : {
      "LABEL" : "User role selected",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "harvard_pnl_rules_user_selected_role" ],
      "DO" : [
        { "drupal_message" : { "message" : "User role selected" } },
        { "redirect" : { "url" : "user\\/[site:current-user:uid]\\/edit" } }
      ]
    }
  }');
  $items['rules_user_updated_profile_trainee_or_faculty_'] = entity_import('rules_config', '{ "rules_user_updated_profile_trainee_or_faculty_" : {
      "LABEL" : "Trainee user updated profile (was NOT changed Supervising Faculty Member and Expiry date)",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "harvard_pnl" ],
      "ON" : [ "user_presave" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } },
        { "harvard_pnl_rules_condition_is_current_user_profile_updated" : { "account" : [ "account" ] } },
        { "NOT harvard_pnl_rules_condition_has_been_cnanged_supervising" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        },
        { "NOT harvard_pnl_rules_condition_has_been_cnanged_expiry_date" : {
            "account" : [ "account" ],
            "account_unchanged" : [ "account_unchanged" ]
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Page Updated \\r\\nTrainee member (not changed supervision faculty)" } },
        { "harvard_pnl_user_set_update_date" : { "account" : [ "account" ] } }
      ]
    }
  }');
  return $items;
}
