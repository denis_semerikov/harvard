<?php
/**
 * @file
 * people_and_labs.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function people_and_labs_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'people_and_labs';
  $ds_view_mode->label = 'people_and_labs';
  $ds_view_mode->entities = array(
    'user' => 'user',
  );
  $export['people_and_labs'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'short_user_info';
  $ds_view_mode->label = 'short_user_info';
  $ds_view_mode->entities = array(
    'user' => 'user',
  );
  $export['short_user_info'] = $ds_view_mode;

  return $export;
}
