<?php
/**
 * @file
 * people_and_labs.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function people_and_labs_user_default_permissions() {
  $permissions = array();

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
      3 => 'anonymous user',
      4 => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: create field_account_academic_degree.
  $permissions['create field_account_academic_degree'] = array(
    'name' => 'create field_account_academic_degree',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_approve_status.
  $permissions['create field_account_approve_status'] = array(
    'name' => 'create field_account_approve_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_date_of_approval.
  $permissions['create field_account_date_of_approval'] = array(
    'name' => 'create field_account_date_of_approval',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_expiry_date.
  $permissions['create field_account_expiry_date'] = array(
    'name' => 'create field_account_expiry_date',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_faculty_member.
  $permissions['create field_account_faculty_member'] = array(
    'name' => 'create field_account_faculty_member',
    'roles' => array(
      0 => 'Editor',
      1 => 'Trainee Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_hcat_internal_id.
  $permissions['create field_account_hcat_internal_id'] = array(
    'name' => 'create field_account_hcat_internal_id',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_huid.
  $permissions['create field_account_huid'] = array(
    'name' => 'create field_account_huid',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_is_new.
  $permissions['create field_account_is_new'] = array(
    'name' => 'create field_account_is_new',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_member_status.
  $permissions['create field_account_member_status'] = array(
    'name' => 'create field_account_member_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_phone.
  $permissions['create field_account_phone'] = array(
    'name' => 'create field_account_phone',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_rejected_notes.
  $permissions['create field_account_rejected_notes'] = array(
    'name' => 'create field_account_rejected_notes',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_account_research_area.
  $permissions['create field_account_research_area'] = array(
    'name' => 'create field_account_research_area',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_member_status.
  $permissions['create field_member_status'] = array(
    'name' => 'create field_member_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_academic_degree.
  $permissions['edit field_account_academic_degree'] = array(
    'name' => 'edit field_account_academic_degree',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_approve_status.
  $permissions['edit field_account_approve_status'] = array(
    'name' => 'edit field_account_approve_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_date_of_approval.
  $permissions['edit field_account_date_of_approval'] = array(
    'name' => 'edit field_account_date_of_approval',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_expiry_date.
  $permissions['edit field_account_expiry_date'] = array(
    'name' => 'edit field_account_expiry_date',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_faculty_member.
  $permissions['edit field_account_faculty_member'] = array(
    'name' => 'edit field_account_faculty_member',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_hcat_internal_id.
  $permissions['edit field_account_hcat_internal_id'] = array(
    'name' => 'edit field_account_hcat_internal_id',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_huid.
  $permissions['edit field_account_huid'] = array(
    'name' => 'edit field_account_huid',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_is_new.
  $permissions['edit field_account_is_new'] = array(
    'name' => 'edit field_account_is_new',
    'roles' => array(
      0 => 'Faculty Member',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_member_status.
  $permissions['edit field_account_member_status'] = array(
    'name' => 'edit field_account_member_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_phone.
  $permissions['edit field_account_phone'] = array(
    'name' => 'edit field_account_phone',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_rejected_notes.
  $permissions['edit field_account_rejected_notes'] = array(
    'name' => 'edit field_account_rejected_notes',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_account_research_area.
  $permissions['edit field_account_research_area'] = array(
    'name' => 'edit field_account_research_area',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_member_status.
  $permissions['edit field_member_status'] = array(
    'name' => 'edit field_member_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_academic_degree.
  $permissions['edit own field_account_academic_degree'] = array(
    'name' => 'edit own field_account_academic_degree',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_approve_status.
  $permissions['edit own field_account_approve_status'] = array(
    'name' => 'edit own field_account_approve_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_date_of_approval.
  $permissions['edit own field_account_date_of_approval'] = array(
    'name' => 'edit own field_account_date_of_approval',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_expiry_date.
  $permissions['edit own field_account_expiry_date'] = array(
    'name' => 'edit own field_account_expiry_date',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_faculty_member.
  $permissions['edit own field_account_faculty_member'] = array(
    'name' => 'edit own field_account_faculty_member',
    'roles' => array(
      0 => 'Editor',
      1 => 'Trainee Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_hcat_internal_id.
  $permissions['edit own field_account_hcat_internal_id'] = array(
    'name' => 'edit own field_account_hcat_internal_id',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_huid.
  $permissions['edit own field_account_huid'] = array(
    'name' => 'edit own field_account_huid',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_is_new.
  $permissions['edit own field_account_is_new'] = array(
    'name' => 'edit own field_account_is_new',
    'roles' => array(
      0 => 'Faculty Member',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_member_status.
  $permissions['edit own field_account_member_status'] = array(
    'name' => 'edit own field_account_member_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_phone.
  $permissions['edit own field_account_phone'] = array(
    'name' => 'edit own field_account_phone',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_rejected_notes.
  $permissions['edit own field_account_rejected_notes'] = array(
    'name' => 'edit own field_account_rejected_notes',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_account_research_area.
  $permissions['edit own field_account_research_area'] = array(
    'name' => 'edit own field_account_research_area',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_member_status.
  $permissions['edit own field_member_status'] = array(
    'name' => 'edit own field_member_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: pnl access to full node list on node path.
  $permissions['pnl access to full node list on node path'] = array(
    'name' => 'pnl access to full node list on node path',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: pnl can cancel member account.
  $permissions['pnl can cancel member account'] = array(
    'name' => 'pnl can cancel member account',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: pnl can change Faculty approve status.
  $permissions['pnl can change Faculty approve status'] = array(
    'name' => 'pnl can change Faculty approve status',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: pnl can change Faculty member for Trainee members.
  $permissions['pnl can change Faculty member for Trainee members'] = array(
    'name' => 'pnl can change Faculty member for Trainee members',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: pnl can change Trainee approve status.
  $permissions['pnl can change Trainee approve status'] = array(
    'name' => 'pnl can change Trainee approve status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: pnl can change expiry date for Trainee members.
  $permissions['pnl can change expiry date for Trainee members'] = array(
    'name' => 'pnl can change expiry date for Trainee members',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: pnl can change member status for Trainee members.
  $permissions['pnl can change member status for Trainee members'] = array(
    'name' => 'pnl can change member status for Trainee members',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'harvard_pnl',
  );

  // Exported permission: view field_account_academic_degree.
  $permissions['view field_account_academic_degree'] = array(
    'name' => 'view field_account_academic_degree',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_approve_status.
  $permissions['view field_account_approve_status'] = array(
    'name' => 'view field_account_approve_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_date_of_approval.
  $permissions['view field_account_date_of_approval'] = array(
    'name' => 'view field_account_date_of_approval',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_expiry_date.
  $permissions['view field_account_expiry_date'] = array(
    'name' => 'view field_account_expiry_date',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_faculty_member.
  $permissions['view field_account_faculty_member'] = array(
    'name' => 'view field_account_faculty_member',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
      3 => 'anonymous user',
      4 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_hcat_internal_id.
  $permissions['view field_account_hcat_internal_id'] = array(
    'name' => 'view field_account_hcat_internal_id',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_huid.
  $permissions['view field_account_huid'] = array(
    'name' => 'view field_account_huid',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_is_new.
  $permissions['view field_account_is_new'] = array(
    'name' => 'view field_account_is_new',
    'roles' => array(
      0 => 'Faculty Member',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_member_status.
  $permissions['view field_account_member_status'] = array(
    'name' => 'view field_account_member_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_phone.
  $permissions['view field_account_phone'] = array(
    'name' => 'view field_account_phone',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
      3 => 'anonymous user',
      4 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_rejected_notes.
  $permissions['view field_account_rejected_notes'] = array(
    'name' => 'view field_account_rejected_notes',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_account_research_area.
  $permissions['view field_account_research_area'] = array(
    'name' => 'view field_account_research_area',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
      4 => 'anonymous user',
      5 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_member_status.
  $permissions['view field_member_status'] = array(
    'name' => 'view field_member_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_academic_degree.
  $permissions['view own field_account_academic_degree'] = array(
    'name' => 'view own field_account_academic_degree',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_approve_status.
  $permissions['view own field_account_approve_status'] = array(
    'name' => 'view own field_account_approve_status',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_date_of_approval.
  $permissions['view own field_account_date_of_approval'] = array(
    'name' => 'view own field_account_date_of_approval',
    'roles' => array(
      0 => 'Editor',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_expiry_date.
  $permissions['view own field_account_expiry_date'] = array(
    'name' => 'view own field_account_expiry_date',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_faculty_member.
  $permissions['view own field_account_faculty_member'] = array(
    'name' => 'view own field_account_faculty_member',
    'roles' => array(
      0 => 'Editor',
      1 => 'Trainee Member',
      2 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_hcat_internal_id.
  $permissions['view own field_account_hcat_internal_id'] = array(
    'name' => 'view own field_account_hcat_internal_id',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_huid.
  $permissions['view own field_account_huid'] = array(
    'name' => 'view own field_account_huid',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'Unselected',
      4 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_is_new.
  $permissions['view own field_account_is_new'] = array(
    'name' => 'view own field_account_is_new',
    'roles' => array(
      0 => 'Faculty Member',
      1 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_member_status.
  $permissions['view own field_account_member_status'] = array(
    'name' => 'view own field_account_member_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_phone.
  $permissions['view own field_account_phone'] = array(
    'name' => 'view own field_account_phone',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_rejected_notes.
  $permissions['view own field_account_rejected_notes'] = array(
    'name' => 'view own field_account_rejected_notes',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_account_research_area.
  $permissions['view own field_account_research_area'] = array(
    'name' => 'view own field_account_research_area',
    'roles' => array(
      0 => 'Editor',
      1 => 'Faculty Member',
      2 => 'Trainee Member',
      3 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_member_status.
  $permissions['view own field_member_status'] = array(
    'name' => 'view own field_member_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
