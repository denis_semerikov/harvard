<?php
/**
 * @file
 * people_and_labs.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function people_and_labs_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:people-labs
  $menu_links['main-menu:people-labs'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'people-labs',
    'router_path' => 'people-labs',
    'link_title' => 'People and Labs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: menu-header-menu-logout:user/logout
  $menu_links['menu-header-menu-logout:user/logout'] = array(
    'menu_name' => 'menu-header-menu-logout',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Logout',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-people-labs:people-labs
  $menu_links['menu-people-labs:people-labs'] = array(
    'menu_name' => 'menu-people-labs',
    'link_path' => 'people-labs',
    'router_path' => 'people-labs',
    'link_title' => 'Overview',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Logout');
  t('Overview');
  t('People and Labs');


  return $menu_links;
}
