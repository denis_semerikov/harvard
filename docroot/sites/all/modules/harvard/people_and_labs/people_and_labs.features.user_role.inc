<?php
/**
 * @file
 * people_and_labs.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function people_and_labs_user_default_roles() {
  $roles = array();

  // Exported role: Announcement Creator.
  $roles['Announcement Creator'] = array(
    'name' => 'Announcement Creator',
    'weight' => '7',
  );

  // Exported role: Editor.
  $roles['Editor'] = array(
    'name' => 'Editor',
    'weight' => '8',
  );

  // Exported role: Event Creator.
  $roles['Event Creator'] = array(
    'name' => 'Event Creator',
    'weight' => '6',
  );

  // Exported role: Faculty Member.
  $roles['Faculty Member'] = array(
    'name' => 'Faculty Member',
    'weight' => '5',
  );

  // Exported role: Reviewer.
  $roles['Reviewer'] = array(
    'name' => 'Reviewer',
    'weight' => '3',
  );

  // Exported role: Trainee Member.
  $roles['Trainee Member'] = array(
    'name' => 'Trainee Member',
    'weight' => '4',
  );

  // Exported role: Unselected.
  $roles['Unselected'] = array(
    'name' => 'Unselected',
    'weight' => '9',
  );

  return $roles;
}
