(function ($) {
  $(function () {
    var lastNameWrap = $('#last-name-wrap', '#people_labs');
    var researchAreaWrap = $('#research-wrap', '#people_labs');
    var searchWrap = $('#search-wrap', '#people_labs');
    var roleFilter = $("#display-by-role-status", '#people_labs');

    var url = Drupal.settings.pnlUrl;
    var args = Drupal.settings.pnlArgs;
    var arg0 = Drupal.settings.pnlArg0;

    var pnlFacultyRoleId = Drupal.settings.pnlFacultyRoleId;
    var pnlTraineeRoleId = Drupal.settings.pnlTraineeRoleId;

    var search_blocks_heights = [];
    search_blocks_heights['lastNameWrap'] = lastNameWrap.height();
    search_blocks_heights['researchAreaWrap'] = researchAreaWrap.height();
    search_blocks_heights['searchWrap'] = searchWrap.height();

    // slide search blocks
    if (args[0] == arg0) {
      if (isset(args[3]) && args[3] != 'all') {
        // slide lastNameWrap block
        searchWrap.addClass('pnl-search-block-hidden');
        searchWrap.addClass('pnl-search-result-page');
        searchWrap.prepend('<div class="pnl-search-options-wrap"><a href="#" class="pnl-search-options">' + Drupal.t("View search options") + '</a><span></span></div>');
        searchWrap.find('.pnl-search-options').data('status', 'off');
      } else if (isset(args[2]) && args[2] != 'all') {
        // slide lastNameWrap block
        researchAreaWrap.addClass('pnl-search-block-hidden');
        researchAreaWrap.addClass('pnl-search-result-page');
        researchAreaWrap.prepend('<div class="pnl-search-options-wrap"><a href="#" class="pnl-search-options">' + Drupal.t("View search options") + '</a><span></span></div>');
        researchAreaWrap.find('.pnl-search-options').data('status', 'off');
      } else if (isset(args[1]) && args[1] != 'all') {
        // slide lastNameWrap block
        lastNameWrap.addClass('pnl-search-block-hidden');
        lastNameWrap.addClass('pnl-search-result-page');
        lastNameWrap.prepend('<div class="pnl-search-options-wrap"><a href="#" class="pnl-search-options">' + Drupal.t("View search options") + '</a><span></span></div>');
        lastNameWrap.find('.pnl-search-options').data('status', 'off');
      }
   }


    // slide search events
    $(".pnl-search-options").live('click',function(){
      var status = $(this).data('status');
      if (status == 'off') {
        $(this).text(Drupal.t("Hide search options"));
        $(this).data('status', 'on');
        $(this).parents(".pnl-search-filter-block").removeClass('pnl-search-block-hidden');
      } else {
        $(this).text(Drupal.t("View search options"));
        $(this).data('status', 'off');
        $(this).parents(".pnl-search-filter-block").addClass('pnl-search-block-hidden');
      }
      return false;
    });

  });

})(jQuery);



function isset() {
  var a=arguments, l=a.length, i=0;
  if (l===0) {
    throw new Error('Empty isset');
  }

  while (i!==l) {
    if (typeof(a[i])=='undefined' || a[i]===null) {
      return false;
    } else {
      i++;
    }
  }
  return true;
}

function empty(mixed_var) {
  if (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || typeof mixed_var === 'undefined') {
    return true;
  }

  if (typeof mixed_var == 'object') {
    for (key in mixed_var) {
      return false;
    }
    return true;
  }
  return false;
}