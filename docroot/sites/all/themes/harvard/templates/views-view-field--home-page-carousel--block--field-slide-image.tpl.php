<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */

?>
<?php 
  $external = FALSE;
  if (isset($row->field_field_external_url[0]['raw']['url']) && !empty($row->field_field_external_url[0]['raw']['url'])) {
    //use external
    $url = $row->field_field_external_url[0]['raw']['url'];
    $external = TRUE;
  }
  else {//use nodereference
    $url = url('node/' . $row->field_field_slide_node_reference[0]['raw']['target_id']);
  }

  print '<a href="' . $url . '"' . ($external ? ' target="_blank"' : '') . '>' . render($row->field_field_slide_image) . '</a>';

?>