<?php
if (isset($content['field_events_location']['#items'][0]['value'])) {
  print $content['field_events_location']['#items'][0]['value'];
}
?>
<br>
<?php print $content['field_events_date'][0]['#markup']; ?>
<?php
if (isset($content['field_events_sponsor_institution'][0]['#markup'])) {
  print '<br><b>Sponsor/Institution: </b>' . $content['field_events_sponsor_institution'][0]['#markup'];
}
?>
<?php
if (isset($content['field_events_contact_info'][0]['#markup'])) {
  print '<br><b>Contact information: </b>' . strip_tags($content['field_events_contact_info']['#items'][0]['value'], '<a><span>');
}
?>
<br>
<?php print $content['body'][0]['#markup']; ?>
<?php
if (isset($content['field_events_more_info'][0]['#markup'])) {
  print '<b>More info: </b>' . $content['field_events_more_info'][0]['#markup'];
}
?>

