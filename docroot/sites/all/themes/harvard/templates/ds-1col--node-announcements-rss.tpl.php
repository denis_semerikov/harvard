<?php
if (isset($content['field_announcements_subtitle']['#items'][0]['value'])) {
  print $content['field_announcements_subtitle']['#items'][0]['value'];
}
?>
<br>
<?php print $content['field_announcements_date'][0]['#markup']; ?>
<?php
if (isset($content['field_announcement_sponsoring_in'][0]['#markup'])) {
  print '<br><b>Sponsoring Institution: </b>' . $content['field_announcement_sponsoring_in'][0]['#markup'];
}
?>
<br>
<?php print $content['body'][0]['#markup']; ?>