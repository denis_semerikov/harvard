<div class="profile-teaser">
  <div class="user-profile-row1">

    <?php
      $account = $elements['#account'];
      if (!empty($account->uid) && user_access('access user profiles') && !empty($user_profile['user_picture']['#markup'])) {
        $attributes = array('attributes' => array('title' => t('View user profile.')), 'html' => TRUE);
        $user_profile['user_picture'] = l($user_profile['user_picture']['#markup'], "user/$account->uid", $attributes);
      }
    ?>
    <?php print render($user_profile['user_picture']); ?>
  </div>
  <div class="user-profile-row2">
    <div class="user-name">
    <?php
      $name = render($user_profile['field_account_last_name']) . '<span>,</span>' .
            render($user_profile['field_account_first_name']) . ' ' .
            render($user_profile['field_account_middle_initial']);
      print l($name, 'user/' . $elements['#account']->uid, array('html' => TRUE));
    ?>

   </div>

    <div class="user-title">
      <?php print render($user_profile['field_account_title']); ?>
    </div>

    <div class="user-institution">
      <?php print render($user_profile['field_account_institution']); ?>
    </div>

    <div class="user-profile-research-areas"><?php print render($user_profile['field_account_research_area']);?></div>

  </div>
  <div class="clear-both"></div>
</div>
