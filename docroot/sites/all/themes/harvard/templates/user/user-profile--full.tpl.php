<div class="profile<?php print empty($user_profile['user_picture']['#markup']) ? ' no-picture' : '' ;?>">
  <?php
  $account = $elements['#account'];
  ?>
  <div class="user-profile-row1">

    <?php if ($account->uid == $user->uid || $user->uid == 1): ?>
    <?php endif; ?>
    <?php print render($user_profile['user_picture']); ?>
    <div class="user-name">

      <?php
      $name = render($user_profile['field_account_first_name']) . ' ' .
          render($user_profile['field_account_middle_initial']) . ' ' .
          render($user_profile['field_account_last_name']);
      print $name;
      ?>
      <div class="user-profile-email-wrapper field">
        <?php if ($ad = render($user_profile['field_account_academic_degree'])) : ?><span>,</span> <?php print l($ad, 'user/' . $elements['#account']->uid, array('html' => TRUE)); ?><?php endif; ?>
      </div>
    </div>
    <?php if(!empty($account->profile_edit_link)): ?>
      <div class="edit-profile-link">
        <?php print $account->profile_edit_link; ?>
      </div>
    <?php endif; ?>
    <div class="user-title">
      <?php print render($user_profile['field_account_title']); ?>
    </div>

    <div class="user-institution">
      <?php print render($user_profile['field_account_institution']); ?>
    </div>

  </div>
  <div class="user-profile-row2">
    <div class="user-profile-row2-left-wrapper">
      <div class="user-profile-row2-left">
        <?php print render($user_profile['field_account_department']); ?>
        <?php print render($user_profile['field_account_division']); ?>
        <?php print render($user_profile['field_account_address']); ?>
        <?php print render($user_profile['field_account_phone']); ?>
        <?php print render($user_profile['field_account_fax']); ?>

        <?php if (isset($user_profile['field_account_display_email']['#items'][0]['value']) && !empty($user_profile['field_account_display_email']['#items'][0]['value'])) : ?>
          <?php print t("Email: "); ?> <img src="<?php print url('user/get-email/' . $account->uid); ?>" class="user-profile-email"/>
        <?php endif; ?>

        <?php if (isset($account->students) && !empty($account->students)): ?>
          <div class="user-profile-students">
            <div class="user-profile-students-label"><?php print t('Fellows and students'); ?></div>
            <ul>
              <?php foreach ($account->students as $student): ?>
                <li>
                  <?php
                  $name = $student->first_name . ' ' .
                          $student->middle_initial . ' ' .
                          $student->last_name;
                  print l($name, 'user/' . $student->uid, array('html' => TRUE));
                  ?>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
      </div>
      <div class="user-profile-faculty-member">
        <?php print render($user_profile['field_account_faculty_member']); ?>
      </div>
    </div>
    <div class="user-profile-row2-right">
      <?php if(!empty($user_profile['field_account_website_url'])): ?>
       <div class="user-profile-websites"><?php print render($user_profile['field_account_website_url']); ?></div>
      <?php endif; ?>
      <div class="user-profile-research-areas"><?php print render($user_profile['field_account_research_area']); ?></div>
    </div>
  </div>

</div>