<?php
define('PEOPLE_AND_LABS_URL', 'people-labs');

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

function harvard_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = 7; //$variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('<< First Page')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('< Previous Page')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('Next Page >')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('Last Page >>')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '...',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '...',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

/**
 * Implemnets template_preprocess_user_profile().
 */
function harvard_preprocess_user_profile(&$variables){
  if ($variables['elements']['#view_mode'] == 'people_and_labs_full' ||
      $variables['elements']['#view_mode'] == 'full') {
    menu_set_active_item(PEOPLE_AND_LABS_URL);
  }

  $view_mode = (isset($variables['elements']['#view_mode'])) ? $variables['elements']['#view_mode'] : NULL;
  if ($view_mode) {
    $variables['theme_hook_suggestions'][] = 'user_profile__' . $view_mode;
  }
}

/**
 * Implemnets template_preprocess_user_picture().
 */
function harvard_preprocess_user_picture(&$variables){
    $variables['user_picture'] = '';
  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if (!empty($account->picture)) {
      if (is_numeric($account->picture)) {
        $account->picture = file_load($account->picture);
      }
      if (!empty($account->picture->uri)) {
        $filepath = $account->picture->uri;
      }
    }
    elseif (variable_get('user_picture_default', '')) {
      $filepath = variable_get('user_picture_default', '');
    }
    if (isset($filepath)) {
      $alt = t("@user's picture", array('@user' => format_username($account)));
      // If the image does not have a valid Drupal scheme (for eg. HTTP),
      // don't load image styles.
      if (module_exists('image') && file_valid_uri($filepath) && $style = variable_get('user_picture_style', '')) {
        $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      else {
        $variables['user_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
    }
  }
}

/**
 * Implemnets template_preprocess_page().
 */
function harvard_preprocess_page(&$variables){
  if (in_array('page__people_labs', $variables['theme_hook_suggestions'])) {
    drupal_add_js(array('pnlUrl' => url(PEOPLE_AND_LABS_URL), 'pnlArgs' => arg(), 'pnlArg0' => PEOPLE_AND_LABS_URL), 'setting');
    drupal_add_js(base_path() . drupal_get_path('theme', 'harvard') . '/js/harvard_pnl.js');
    if (module_enable(array('harvard_pnl'))){
      drupal_add_js(array(
        'pnlFacultyRoleId' => FACULTY_ROLE_ID,
        'pnlTraineeRoleId' => TRAINEE_ROLE_ID
        ), 'setting');
    }
    $variables['title_hidden'] = 1;
  }
  if (in_array('page__user', $variables['theme_hook_suggestions'])) {
    $variables['title_hidden'] = 1;
  }
  if (in_array('page__user__edit', $variables['theme_hook_suggestions'])) {
    $user = user_load($variables['user']->uid);
    $is_new = (field_get_items('user', $user, 'field_account_is_new'));
    if( !empty($is_new) && ($is_new[0]['value'] == 1) ){
      drupal_set_title(t('Request a New Member'));
    }else{
      drupal_set_title(t('Edit Member'));
    }
  }
  if(module_enable(array('harvard_pnl'))){
    $variables['page']['content']['login_buttons'] = array('#type' => 'markup', '#markup' => harvard_pnl_emulate_huid_login_through_api(),'#weight' => -999);
  };
}

/**
 * Implemnets template_preprocess_panels_pane().
 */
function harvard_preprocess_panels_pane(&$vars) {
  $pane = $vars['pane'];
  $subtype = $pane->subtype;
  $layout = $vars['display']->layout;

  // generate new custom preprocess
  $preprocess = array(
    'harvard_preprocess_panels_pane_' . str_replace('-', '_', $layout),
    'harvard_preprocess_panels_pane_' . str_replace('-', '_', $subtype),
    'harvard_preprocess_panels_pane_' . str_replace('-', '_', $layout) . '__' . str_replace('-', '_', $subtype),
  );
  foreach (array_reverse($preprocess) as $function) {
    if (function_exists($function)) {
      $function($vars);
      break;
    }
  }
}

/**
 * Implemnets template_preprocess_panels_pane_subtype().
 *
 * Browse By Last Name
 */
function harvard_preprocess_panels_pane_block_10(&$vars){
  $args = arg();
  $content =  theme('browser_by_last_name_links', array('url' => PEOPLE_AND_LABS_URL));

  $args[1] = isset($args[1]) ? $args[1] : NULL;
  if(!empty($args[1])){
    $content .=  theme('new_search_link', array('url' => PEOPLE_AND_LABS_URL));
  }
  $vars['content'] .= $content;
  $vars['title'] = "<span>" . t('Browse By') . "</span> " . t('Last Name');
}

/**
 * Implemnets template_preprocess_panels_pane_subtype().
 *
 * Browse By Research Area
 */
function harvard_preprocess_panels_pane_block_11(&$vars){
  $content = (module_enable(array('harvard_pnl'))) ? drupal_render(drupal_get_form('harvard_pnl_browse_by_research_area_form')) : '';
  $args = arg();
  $args[1] = isset($args[1]) ? $args[1] : NULL;

  if(!empty($args[1])){
    $content .=  theme('new_search_link', array('url' => PEOPLE_AND_LABS_URL));
  }
  $vars['content'] .= $content;
  $vars['title'] = "<span>" . t('Browse By') . "</span> " . t('Research Area');
}


/**
 * Implements template_preprocess_field().
 */
function harvard_preprocess_field(&$vars, $hook) {
  // Add line breaks to plain text textareas.
  if ($vars['element']['#field_name'] == 'field_account_address'){
    $vars['items'][0]['#markup'] = nl2br($vars['items'][0]['#markup']);
  }
}
/**
* Implements hook_process_region().
*/
function harvard_process_region(&$vars) {
  if ($vars['region'] == 'content'){

  }
}